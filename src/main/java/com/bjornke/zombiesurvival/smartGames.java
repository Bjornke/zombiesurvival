/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class smartGames {

    public int smartWaveCount(String map) {
        int mz = games.getMaxZombies(map);
        int w = games.getWave(map);
        int pre = 1;
        int fin = 1;
        if(mz < 10) {
            pre = (int)(mz * w * 0.5);
        }
        if(mz >= 10 && mz <= 50) {
            pre = (int)(mz * w * 0.1);
        }
        if(mz >= 51 && mz <= 100) {
            pre = (int)(mz * w * 0.08);
        }
        if(mz >= 101 && mz <= 200) {
            pre = (int)(mz * w * 0.05);
        }
        if(mz >= 201) {
            pre = (int)(mz * w * 0.04);
        }
        if(pre < 1) {
            pre = 1;
        }
        fin = (int)(pre * (pmethods.numberInMap(map) / games.getMaxPlayers(map)));
        if(fin < (pre * 0.6)) {
            fin = (int)(pre * 0.6);
        }
        if(fin < 1) {
            fin = 1;
        }
        return fin;
    }

}
