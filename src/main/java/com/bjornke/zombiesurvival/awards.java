package com.bjornke.zombiesurvival;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class awards {

    Plugin plugin;
    private Map<String, Integer> indKills = new HashMap<String, Integer>();
    private Map<Integer, ItemStack> rewardItems = new HashMap<Integer, ItemStack>();
    private Map<Integer, String> rewardMessage = new HashMap<Integer, String>();
    private FileConfiguration rConfig = null;
    private File rConfigFile = null;

    public awards(main instance) {
        plugin = instance;
    }

    public void LoadAwards() {
        for(int i : getRConfig().getIntegerList("award-at-kill")) {
            try {
                String[] line = getRConfig().getString(Integer.toString(i) + ".item").split(":");
                int itemid = Integer.parseInt(line[0]);
                short itemdamage = 0;
                if(line.length > 1) {
                    itemdamage = Short.parseShort(line[1]);
                }
                ItemStack item = new ItemStack(itemid, 1, itemdamage);
                rewardItems.put(i, item);
                rewardMessage.put(i, getRConfig().getString(Integer.toString(i) + ".message"));
                resetKills();
            } catch(Exception e) {
                plugin.getLogger().warning("Could not load award for kill: " + Integer.toString(i));
            }
        }
    }

    public void addKill(Player p) {
        String name = p.getName();
        if(indKills.containsKey(name)) {
            indKills.put(name, indKills.get(name) + 1);
        } else {
            indKills.put(name, 1);
        }
    }

    public void callAward(String name) {
        //Find highest reward possible
        Player p = Bukkit.getPlayer(name);
        int ir = 0;
        for(int i : rewardItems.keySet()) {
            if(indKills.get(name) >= i) {
                if(i > ir) {
                    ir = i;
                }
            }
        }
        if(rewardItems.containsKey(ir)) {
            p.getInventory().addItem(rewardItems.get(ir));
        }
        if(rewardMessage.containsKey(ir)) {
            p.sendMessage(ChatColor.DARK_PURPLE + rewardMessage.get(ir));
        }
    }

    public void resetKills() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            public void run() {
                for(String p : indKills.keySet()) {
                    callAward(p);
                }
                indKills.clear();
            }

        }, 40, 40);
    }

    public void saveRConfig() {
        if(rConfig == null || rConfigFile == null) {
            return;
        }
        try {
            getRConfig().save(rConfigFile);
        } catch(IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + rConfigFile, ex);
        }
    }

    public FileConfiguration getRConfig() {
        if(rConfig == null) {
            reloadRConfig();
        }
        return rConfig;
    }

    public void reloadRConfig() {
        if(rConfigFile == null) {
            rConfigFile = new File(plugin.getDataFolder(), "awards.yml");
        }
        rConfig = YamlConfiguration.loadConfiguration(rConfigFile);
        InputStream defConfigStream = plugin.getResource("awards.yml");
        if(defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            rConfig.setDefaults(defConfig);
        }
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
