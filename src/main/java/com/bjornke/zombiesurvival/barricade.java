/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.Serializable;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class barricade implements Serializable {

    public double x;
    public double y;
    public double z;
    public byte data;
    public int type;
    public String world;
    public String map;
    public int health;
    public transient Location location;

    public barricade(Block b, String m) {
        x = b.getX();
        y = b.getY();
        z = b.getZ();
        map = m;
        world = b.getWorld().getName();
        location = b.getLocation();
        data = b.getData();
        type = b.getTypeId();
        health = 50;
    }

    public void init() {
        World w = Bukkit.getWorld(world);
        location = new Location(w, x, y, z);
        Block b = location.getBlock();
        b.setData(data);
        b.setTypeId(type);
    }

}
