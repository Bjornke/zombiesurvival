/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class donator implements Listener {

    public Map<String, ItemStack[]> dcontents = new HashMap<String, ItemStack[]>();
    public Map<String, ItemStack[]> darmor = new HashMap<String, ItemStack[]>();

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if(p.hasPermission("zs.donator") && pmethods.inGame(p)) {
            dcontents.put(p.getName(), p.getInventory().getContents());
            darmor.put(p.getName(), p.getInventory().getArmorContents());
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        if(dcontents.containsKey(p.getName())) {
            p.getInventory().setContents(dcontents.get(p.getName()));
            dcontents.remove(p.getName());
        }
        if(darmor.containsKey(p.getName())) {
            p.getInventory().setArmorContents(darmor.get(p.getName()));
            darmor.remove(p.getName());
        }
    }

}
