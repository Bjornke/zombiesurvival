/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author Owner
 */
public class signs implements Listener {

    @EventHandler
    public void onPlayerClick(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK && player.hasPermission("zs.signs")) {
            Block block = e.getClickedBlock();
            if(block.getState() instanceof Sign) {
                Sign sign = (Sign)block.getState();
                String[] lines = sign.getLines();
                if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].equalsIgnoreCase("How To Play")) {
                    player.sendMessage(ChatColor.GRAY + "Step 1: Join a game by either right clicking a stats sign, or using the command /join (may be /zs-join depending on server).");
                    player.sendMessage(ChatColor.GRAY + "Step 2: If you notice items in your inventory, equip them. If not look for a zombie and begin punching!");
                    player.sendMessage(ChatColor.GRAY + "Step 3: Play co-operately and good luck!");
                } else if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].equalsIgnoreCase("What's a Class")) {
                    player.sendMessage(ChatColor.GRAY + "Classes are sets of user abilities. They include tool kits, and special powers.");
                } else if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].equalsIgnoreCase("Top Players")) {
                    player.sendMessage(ChatColor.GRAY + "The Top Scoring Player: " + stats.topScorePlayer() + " - " + Double.toString(stats.topScore()));
                    player.sendMessage(ChatColor.GRAY + "The Most Savage Killer: " + stats.topKillsPlayer() + " - " + Double.toString(stats.topKills()));
                    player.sendMessage(ChatColor.GRAY + "The Dead Guy: " + stats.topDeathsPlayer() + " - " + Double.toString(stats.topDeaths()));
                } else if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].equalsIgnoreCase("My Top Scores")) {
                    player.sendMessage(ChatColor.GREEN + "Total Points: " + ChatColor.DARK_RED + Double.toString(stats.getTotalPoints(player.getName())));
                    player.sendMessage(ChatColor.GREEN + "Total Kills: " + ChatColor.DARK_RED + Double.toString(stats.getTotalKills(player.getName())));
                    player.sendMessage(ChatColor.GREEN + "Total Deaths: " + ChatColor.DARK_RED + Double.toString(stats.getTotalDeaths(player.getName())));
                } else if(lines[0].equalsIgnoreCase("§9zombie class") && lines[1].equalsIgnoreCase("Join")) {
                    if(!pmethods.inGame(player)) {
                        classes.setUserClass(player, lines[2]);
                    } else {
                        player.sendMessage(ChatColor.RED + "You cannot change classes while in-game.");
                    }
                } else if(lines[0].equalsIgnoreCase("§9zombie class") && lines[1].equalsIgnoreCase("Leave")) {
                    classes.removeUser(player);
                    player.sendMessage(ChatColor.GRAY + "You have left your class");
                }
            }
        } else if(e.getAction() == Action.LEFT_CLICK_BLOCK && player.hasPermission("zs.signs")) {
            Block block = e.getClickedBlock();
            if(block.getState() instanceof Sign) {
                Sign sign = (Sign)block.getState();
                String[] lines = sign.getLines();
                if(lines[0].equalsIgnoreCase("§dzombie stats")) {
                    player.sendMessage(ChatColor.GRAY + "Right click this sign to join a game!");
                    if(games.exists(lines[1])) {
                        player.sendMessage(ChatColor.GREEN + "Players: " + ChatColor.DARK_RED + Integer.toString(pmethods.numberInMap(lines[1])) + ChatColor.GRAY + "/" + ChatColor.DARK_GREEN + Integer.toString(games.getMaxPlayers(lines[1])));
                        player.sendMessage(ChatColor.GREEN + "Players Alive: " + ChatColor.DARK_RED + Integer.toString(games.getPcount(lines[1])) + ChatColor.GRAY + "/" + ChatColor.DARK_GREEN + Integer.toString(pmethods.numberInMap(lines[1])));
                        player.sendMessage(ChatColor.GREEN + "Wave: " + ChatColor.DARK_RED + Integer.toString(games.getWave(lines[1])));
                        player.sendMessage(ChatColor.GREEN + "Zombies: " + ChatColor.DARK_RED + Integer.toString(games.getZcount(lines[1])));
                        player.sendMessage(ChatColor.GREEN + "Zombies Remaining: " + ChatColor.DARK_RED + Integer.toString(games.getZcount(lines[1]) - games.getZslayed(lines[1])));
                        player.sendMessage(ChatColor.GREEN + "Wave Max Zombies: " + ChatColor.DARK_RED + Integer.toString(games.getMaxZombies(lines[1])));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        String[] lines = e.getLines();
        if(lines[0].equalsIgnoreCase("zombie") && !lines[1].isEmpty()) {
            Player player = e.getPlayer();
            e.setLine(0, "§9zombie");
            if(player.hasPermission("zs.edit")) {
                player.sendMessage(ChatColor.GREEN + "You have created a new ZombieSurvival sign!");
            } else {
                e.setLine(0, "§4MUST BE OP");
            }
        } else if(lines[0].equalsIgnoreCase("zombie")) {
            e.setLine(0, "§4BAD SIGN");
        }
        if(lines[0].equalsIgnoreCase("zombie box") && !lines[1].isEmpty()) {
            Player player = e.getPlayer();
            e.setLine(0, "§9zombie box");
            if(player.hasPermission("zs.edit")) {
                player.sendMessage(ChatColor.GREEN + "You have created a new ZombieSurvival sign!");
            } else {
                e.setLine(0, "§4MUST BE OP");
            }
        } else if(lines[0].equalsIgnoreCase("zombie box")) {
            e.setLine(0, "§4BAD SIGN");
        }
        if(lines[0].equalsIgnoreCase("zombie class") && !lines[1].isEmpty()) {
            Player player = e.getPlayer();
            e.setLine(0, "§9zombie class");
            if(player.hasPermission("zs.edit")) {
                player.sendMessage(ChatColor.GREEN + "You have created a new ZombieSurvival sign!");
            } else {
                e.setLine(0, "§4MUST BE OP");
            }
        } else if(lines[0].equalsIgnoreCase("zombie box")) {
            e.setLine(0, "§4BAD SIGN");
        }
    }

}
