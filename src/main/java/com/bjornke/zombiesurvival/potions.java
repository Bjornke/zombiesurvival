/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class potions {

    Plugin plugin;

    public potions(Plugin instance) {
        plugin = instance;
    }

    Random random = new Random();
    language lang = new language(plugin);
    public int effectchance = 20;
    public int bitelength = 10;
    public boolean pot1 = true;
    public boolean pot2 = true;
    public boolean pot3 = true;
    public boolean pot4 = true;
    public boolean pot5 = true;
    public boolean pot6 = true;
    public boolean pot7 = true;

    public void checkForBite(Player p) {
        if(effectchance != 0) {
            int doeffect = (random.nextInt(effectchance) + 1);
            if(doeffect == 1) {
                PotionEffect potion1 = new PotionEffect(PotionEffectType.BLINDNESS, bitelength, 1);
                PotionEffect potion2 = new PotionEffect(PotionEffectType.CONFUSION, bitelength, 1);
                PotionEffect potion3 = new PotionEffect(PotionEffectType.SLOW, bitelength, 1);
                PotionEffect potion4 = new PotionEffect(PotionEffectType.WEAKNESS, bitelength, 1);
                PotionEffect potion5 = new PotionEffect(PotionEffectType.POISON, (int)(bitelength / 3), 1);
                PotionEffect potion6 = new PotionEffect(PotionEffectType.HUNGER, bitelength, 1);
                PotionEffect potion7 = new PotionEffect(PotionEffectType.JUMP, bitelength, 1);
                List<PotionEffect> potions = new ArrayList<PotionEffect>();
                if(pot1) {
                    potions.add(potion1);
                }
                if(pot2) {
                    potions.add(potion2);
                }
                if(pot3) {
                    potions.add(potion3);
                }
                if(pot4) {
                    potions.add(potion4);
                }
                if(pot5) {
                    potions.add(potion5);
                }
                if(pot6) {
                    potions.add(potion6);
                }
                if(pot7) {
                    potions.add(potion7);
                }
                int potionget = random.nextInt(potions.size());
                p.addPotionEffect(potions.get(potionget));
                p.sendMessage(lang.strings.get(69));
            }
        }
    }

    public void LoadPotions() {
        pot1 = plugin.getConfig().getBoolean("Effects.blindness");
        pot2 = plugin.getConfig().getBoolean("Effects.confusion");
        pot3 = plugin.getConfig().getBoolean("Effects.slowness");
        pot4 = plugin.getConfig().getBoolean("Effects.weakness");
        pot5 = plugin.getConfig().getBoolean("Effects.poison");
        pot6 = plugin.getConfig().getBoolean("Effects.hungerpoison");
        pot7 = plugin.getConfig().getBoolean("Effects.jump");
        lang.plugin = plugin;
        lang.LoadLanguage();
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
