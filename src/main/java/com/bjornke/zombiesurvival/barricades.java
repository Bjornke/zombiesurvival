/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class barricades {

    Plugin plugin;
    public List<barricade> bar = new ArrayList<barricade>();

    public void saveBar() {
        try {
            File file = new File(plugin.getDataFolder(), "barricades.zss");
            FileOutputStream saveFile = new FileOutputStream(file);
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(bar);
            save.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void loadBar() {
        try {
            File file = new File(plugin.getDataFolder(), "barricades.zss");
            FileInputStream saveFile = new FileInputStream(file);
            ObjectInputStream restore = new ObjectInputStream(saveFile);
            bar = (ArrayList)restore.readObject();
            restore.close();
            for(barricade d : bar) {
                d.init();
            }
        } catch(Exception e) {
        }
    }

    public void addBar(Block b, String m) {
        barricade newdr = new barricade(b, m);
        bar.add(newdr);
        showBar(newdr);
        saveBar();
    }

    public void removeBar(barricade d) {
        Block b = d.location.getBlock();
        b.setTypeId(d.type);
        b.setData(d.data);
        bar.remove(d);
        saveBar();
    }

    public void showBarricades(String map) {
        for(barricade d : bar) {
            if(d.map.matches(map)) {
                Block b = d.location.getBlock();
                b.setTypeId(35);
                b.setData(DyeColor.LIME.getWoolData());
            }
        }
    }

    public void hideBarricades(String map) {
        for(barricade d : bar) {
            if(d.map.matches(map)) {
                Block b = d.location.getBlock();
                b.setTypeId(d.type);
                b.setData(d.data);
            }
        }
    }

    public void showBar(barricade d) {
        Block b = d.location.getBlock();
        b.setTypeId(35);
        b.setData(DyeColor.LIME.getWoolData());
    }

    public void hideBar(barricade d) {
        Block b = d.location.getBlock();
        b.setTypeId(d.type);
        b.setData(d.data);
    }

    public barricade findBar(double x, double y, double z) {
        for(barricade d : bar) {
            if(d.x == x && d.y == y && d.z == z) {
                return d;
            }
        }
        return null;
    }

    public void damageBarricade(barricade d) {
        d.health--;
        if(d.health <= 0) {
            d.health = 0;
            Block b = d.location.getBlock();
            b.setTypeId(0);
        }
    }

    public boolean healBarricade(barricade d) {
        if(d.health >= 50) {
            return false;
        }
        d.health += 10;
        if(d.health >= 50) {
            d.health = 50;
            Block b = d.location.getBlock();
            b.setTypeId(d.type);
            b.setData(d.data);
        }
        return true;
    }

    public List<barricade> checkForBarricades(Location middle, String map) {
        List<barricade> barshere = new ArrayList<barricade>();
        for(int x = middle.getBlockX() - 3; x <= middle.getBlockX() + 3; x++) {
            for(int y = middle.getBlockY() - 3; y <= middle.getBlockY() + 3; y++) {
                for(int z = middle.getBlockZ() - 3; z <= middle.getBlockZ() + 3; z++) {
                    barricade d = findBar(x, y, z);
                    if(d != null && d.map.matches(map)) {
                        barshere.add(d);
                    }
                }
            }
        }
        return barshere;
    }

    public boolean healBars(Location middle, String map) {
        boolean pay = false;
        for(barricade d : checkForBarricades(middle, map)) {
            if(healBarricade(d) && !pay) {
                pay = true;
            }
        }
        return pay;
    }

    public void damageBars(Location middle, String map) {
        for(barricade d : checkForBarricades(middle, map)) {
            damageBarricade(d);
        }
    }

    public void resetBars(String map) {
        for(barricade d : bar) {
            if(d.map.matches(map)) {
                Block b = d.location.getBlock();
                b.setTypeId(d.type);
                b.setData(d.data);
                d.health = 50;
            }
        }
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
