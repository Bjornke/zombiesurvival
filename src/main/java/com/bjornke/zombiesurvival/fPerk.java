package com.bjornke.zombiesurvival;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class fPerk implements Listener {

    Random random = new Random();
    private Map<String, Location> dLoc = new HashMap<String, Location>();
    private Map<String, Integer> perkID = new HashMap<String, Integer>();
    private Map<String, Integer> gameperks = new HashMap<String, Integer>();
    public boolean isDropped = false;

    public void addLoc(String map, Location l) {
        dLoc.put(map, l);
    }

    public void removeLoc(String map) {
        dLoc.remove(map);
    }

    public void setPerk(String map, int i) {
        gameperks.put(map, i);
    }

    public int getPerk(String map) {
        return gameperks.get(map);
    }

    public Set<String> setPerk() {
        return gameperks.keySet();
    }

    public void callPerk(String map) {
        if(dLoc.get(map) != null && !isDropped) {
            ItemStack item = new ItemStack(perkItem(), 1);
            World world = Bukkit.getWorld(main.Maps.get(map));
            Item i = world.dropItem(dLoc.get(map), item);
            i.teleport(dLoc.get(map));
            perkID.put(map, i.getEntityId());
            isDropped = true;
            for(String pl : pmethods.playersInMap(map)) {
                Player p = Bukkit.getPlayer(pl);
                if(p != null) {
                    p.sendMessage(ChatColor.DARK_PURPLE + "Perk Available!");
                }
            }
        } else {
            NewPerk(map);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPickUp(PlayerPickupItemEvent e) {
        Player player = e.getPlayer();
        Item im = e.getItem();
        if(pmethods.inGame(player)) {
            String map = pmethods.playerGame(player);
            if(perkID.get(map) != null && im.getEntityId() == perkID.get(map)) {
                e.setCancelled(true);
                int randomperk = im.getItemStack().getTypeId();
                switch(randomperk) {
                    case 266:
                        gameperks.put(map, 1);
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(p != null) {
                                p.sendMessage(ChatColor.DARK_PURPLE + "GODMODE PERK ENABLED");
                            }
                        }
                        break;
                    case 264:
                        gameperks.put(map, 2);
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(p != null) {
                                p.sendMessage(ChatColor.DARK_PURPLE + "INSTANT-KILL PERK ENABLED");
                            }
                        }
                        break;
                    case 51:
                        gameperks.put(map, 3);
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(p != null) {
                                p.sendMessage(ChatColor.DARK_PURPLE + "FIRE PERK ENABLED");
                            }
                        }
                        break;
                    case 371:
                        gameperks.put(map, 4);
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(p != null) {
                                p.sendMessage(ChatColor.DARK_PURPLE + "BONUS XP PERK ENABLED");
                            }
                        }
                        break;
                    case 265:
                        gameperks.put(map, 6);
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(p != null) {
                                p.sendMessage(ChatColor.DARK_PURPLE + "IRON FIST PERK ENABLED");
                            }
                        }
                        break;
                }
                im.remove();
                isDropped = false;
            }
        }
    }

    @EventHandler
    public void onDespawn(ItemDespawnEvent e) {
        int id = e.getEntity().getEntityId();
        for(Iterator<String> it = perkID.keySet().iterator(); it.hasNext();) {
            String m = it.next();
            if(perkID.get(m) == id) {
                it.remove();
                isDropped = false;
            }
        }
    }

    public void NewPerk(String map) {
        int randomperk = random.nextInt(5) + 1;
        switch(randomperk) {
            case 1:
                gameperks.put(map, 1);
                for(String pl : pmethods.playersInMap(map)) {
                    Player p = Bukkit.getPlayer(pl);
                    if(p != null) {
                        p.sendMessage(ChatColor.DARK_PURPLE + "GODMODE PERK ENABLED");
                    }
                }
                break;
            case 2:
                gameperks.put(map, 2);
                for(String pl : pmethods.playersInMap(map)) {
                    Player p = Bukkit.getPlayer(pl);
                    if(p != null) {
                        p.sendMessage(ChatColor.DARK_PURPLE + "INSTANT-KILL PERK ENABLED");
                    }
                }
                break;
            case 3:
                gameperks.put(map, 3);
                for(String pl : pmethods.playersInMap(map)) {
                    Player p = Bukkit.getPlayer(pl);
                    if(p != null) {
                        p.sendMessage(ChatColor.DARK_PURPLE + "FIRE PERK ENABLED");
                    }
                }
                break;
            case 4:
                gameperks.put(map, 4);
                for(String pl : pmethods.playersInMap(map)) {
                    Player p = Bukkit.getPlayer(pl);
                    if(p != null) {
                        p.sendMessage(ChatColor.DARK_PURPLE + "BONUS XP PERK ENABLED");
                    }
                }
                break;
            case 5:
                gameperks.put(map, 6);
                for(String pl : pmethods.playersInMap(map)) {
                    Player p = Bukkit.getPlayer(pl);
                    if(p != null) {
                        p.sendMessage(ChatColor.DARK_PURPLE + "IRON FIST PERK ENABLED");
                    }
                }
                break;
        }
    }

    public int perkItem() {
        int randomperk = random.nextInt(5) + 1;
        switch(randomperk) {
            case 1:
                return 266;
            case 2:
                return 264;
            case 3:
                return 51;
            case 4:
                return 371;
            case 5:
                return 265;
        }
        return 0;
    }

}
