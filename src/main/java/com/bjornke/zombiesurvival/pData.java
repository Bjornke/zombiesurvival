/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class pData {

    public String name;
    public int health;
    public int food;
    public Location location;
    public GameMode gamemode;
    public ItemStack[] inventory;
    public ItemStack[] armor;
}
