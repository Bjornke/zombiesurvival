/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class spectate implements Listener {

    static Map<String, String> spectators = new HashMap<String, String>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerClickEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerEggThrow(PlayerEggThrowEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setHatching(false);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPickup(PlayerPickupItemEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPortal(PlayerPortalEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInventory(InventoryOpenEvent e) {
        HumanEntity he = e.getPlayer();
        Player p;
        if(he instanceof Player) {
            p = (Player)he;
        } else {
            return;
        }
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInventory(InventoryClickEvent e) {
        HumanEntity he = e.getWhoClicked();
        Player p;
        if(he instanceof Player) {
            p = (Player)he;
        } else {
            return;
        }
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerExp(PlayerExpChangeEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setAmount(0);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerBreakBlock(BlockBreakEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPlaceBlock(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Entity damager = e.getDamager();
        if(damager instanceof Player) {
            Player p = (Player)damager;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDamage(EntityDamageEvent e) {
        Entity damaged = e.getEntity();
        if(damaged instanceof Player) {
            Player p = (Player)damaged;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerFood(FoodLevelChangeEvent e) {
        Entity damaged = e.getEntity();
        if(damaged instanceof Player) {
            Player p = (Player)damaged;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerCombust(EntityCombustEvent e) {
        Entity damaged = e.getEntity();
        if(damaged instanceof Player) {
            Player p = (Player)damaged;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerShoot(EntityShootBowEvent e) {
        Entity damaged = e.getEntity();
        if(damaged instanceof Player) {
            Player p = (Player)damaged;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerTargetted(EntityTargetEvent e) {
        Entity damaged = e.getTarget();
        if(damaged instanceof Player) {
            Player p = (Player)damaged;
            if(spectators.containsKey(p.getName()) && games.getState(spectators.get(p.getName())) > 1) {
                e.setCancelled(true);
            }
        }
    }

}
