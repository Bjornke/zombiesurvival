/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class doors {

    Plugin plugin;
    public List<door> doors = new ArrayList<door>();

    public doors(Plugin instance) {
        plugin = instance;
    }

    public void saveDoor() {
        try {
            File file = new File(plugin.getDataFolder(), "doors.zss");
            FileOutputStream saveFile = new FileOutputStream(file);
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(doors);
            save.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void loadDoor() {
        try {
            File file = new File(plugin.getDataFolder(), "doors.zss");
            FileInputStream saveFile = new FileInputStream(file);
            ObjectInputStream restore = new ObjectInputStream(saveFile);
            doors = (ArrayList)restore.readObject();
            restore.close();
            for(door d : doors) {
                d.init();
            }
        } catch(Exception e) {
        }
    }

    public void addDoor(Block b, String m, int w) {
        door newdr = new door(b, m, w);
        doors.add(newdr);
        showDoor(newdr);
        saveDoor();
    }

    public void removeDoor(door d) {
        Block b = d.location.getBlock();
        b.setTypeId(d.type);
        b.setData(d.data);
        doors.remove(d);
        saveDoor();
    }

    public void resetDoors(String m) {
        for(door d : doors) {
            if(d.map.matches(m)) {
                Block b = d.location.getBlock();
                b.setData(d.data);
                b.setTypeId(d.type);
                d.open = false;
                deActivateSpawns(d);
            }
        }
    }

    public door findDoor(double x, double y, double z) {
        for(door d : doors) {
            if(d.x == x && d.y == y && d.z == z) {
                return d;
            }
        }
        return null;
    }

    public List<Location> doorLocs(String map) {
        List<Location> locs = new ArrayList<Location>();
        for(door d : doors) {
            if(d.map.matches(map)) {
                if(d.location != null) {
                    locs.add(d.location);
                }
            }
        }
        return locs;
    }

    public List<door> doorWave(String map, int wave) {
        List<door> dwave = new ArrayList<door>();
        for(door d : doors) {
            if(d.map.matches(map) && d.wave == wave) {
                dwave.add(d);
            }
        }
        return dwave;
    }

    public void openDoors(String map, int wave) {
        for(door d : doorWave(map, wave)) {
            Block b = d.location.getBlock();
            b.setTypeId(0);
            d.open = true;
            activateSpawns(d);
        }
    }

    public void openDoor(door d) {
        if(d == null) {
            return;
        }
        Block b = d.location.getBlock();
        b.setTypeId(0);
        d.open = true;
        activateSpawns(d);
    }

    public void showDoors(String map) {
        for(door d : doors) {
            if(d.map.matches(map)) {
                Block b = d.location.getBlock();
                b.setTypeId(35);
                b.setData(DyeColor.LIME.getWoolData());
            }
        }
    }

    public void hideDoors(String map) {
        for(door d : doors) {
            if(d.map.matches(map)) {
                Block b = d.location.getBlock();
                b.setTypeId(d.type);
                b.setData(d.data);
            }
        }
    }

    public void showDoor(door d) {
        Block b = d.location.getBlock();
        b.setTypeId(35);
        b.setData(DyeColor.LIME.getWoolData());
    }

    public void hideDoor(door d) {
        Block b = d.location.getBlock();
        b.setTypeId(d.type);
        b.setData(d.data);
    }

    public void linkDoorSpawn(door d, spawn p) {
        d.spawns.add(p);
    }

    public void activateSpawns(door d) {
        for(spawn s : d.spawns) {
            s.activated = true;
        }
    }

    public void deActivateSpawns(door d) {
        for(spawn s : d.spawns) {
            s.activated = false;
        }
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
