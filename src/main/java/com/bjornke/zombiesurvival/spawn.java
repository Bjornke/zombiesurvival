/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.Serializable;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class spawn implements Serializable {

    public double x;
    public double y;
    public double z;
    public byte data;
    public int type;
    public String world;
    public String map;
    public boolean activated;
    public int wave;
    public transient Location location;

    public spawn(Block b, String m, int w) {
        x = (b.getX() + 0.5);
        y = (b.getY() + 1.0);
        z = (b.getZ() + 0.5);
        map = m;
        activated = false;
        wave = w;
        world = b.getWorld().getName();
        location = b.getLocation().add(0.5, 1.0, 0.5);
        data = b.getRelative(BlockFace.UP).getData();
        type = b.getRelative(BlockFace.UP).getTypeId();
    }

    public void init() {
        World w = Bukkit.getWorld(world);
        location = new Location(w, x, y, z);
        Block b = location.getBlock();
        b.setData(data);
        b.setTypeId(type);
    }

}
