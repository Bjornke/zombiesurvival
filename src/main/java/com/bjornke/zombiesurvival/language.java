/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class language {

    Plugin plugin;

    public language(Plugin instance) {
        plugin = instance;
    }

    private FileConfiguration lConfig = null;
    private File lConfigFile = null;
    List<String> strings = new ArrayList<String>(100);

    public void LoadLanguage() {
        boolean load = true;
        for(int i = 1; i < 80; i++) {
            strings.add(utilities.processForColors(getlConfig().getString(Integer.toString(i))));
        }

    }

    public void savelConfig() {
        if(lConfig == null || lConfigFile == null) {
            return;
        }
        try {
            getlConfig().save(lConfigFile);
        } catch(IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + lConfigFile, ex);
        }
    }

    public FileConfiguration getlConfig() {
        if(lConfig == null) {
            this.reloadlConfig();
        }
        return lConfig;
    }

    public void reloadlConfig() {
        if(lConfigFile == null) {
            lConfigFile = new File(plugin.getDataFolder(), "language.yml");
        }
        lConfig = YamlConfiguration.loadConfiguration(lConfigFile);
        InputStream defConfigStream = plugin.getResource("language.yml");
        if(defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            lConfig.setDefaults(defConfig);
        }
    }

}
