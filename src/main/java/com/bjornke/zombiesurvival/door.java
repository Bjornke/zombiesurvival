/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class door implements Serializable {

    public double x;
    public double y;
    public double z;
    public byte data;
    public int type;
    public int wave;
    public String world;
    public String map;
    public List<spawn> spawns = new ArrayList<spawn>();
    public boolean open;
    public transient Location location;

    public door(Block b, String m, int w) {
        x = b.getX();
        y = b.getY();
        z = b.getZ();
        data = b.getData();
        type = b.getTypeId();
        open = false;
        location = b.getLocation();
        wave = w;
        world = b.getWorld().getName();
        map = m;
    }

    public void init() {
        World w = Bukkit.getWorld(world);
        location = new Location(w, x, y, z);
        open = false;
        Block b = location.getBlock();
        b.setData(data);
        b.setTypeId(type);
    }

}
