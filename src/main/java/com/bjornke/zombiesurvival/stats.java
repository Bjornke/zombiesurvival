package com.bjornke.zombiesurvival;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class stats {

    public Plugin plugin;
    private static Map<String, Double> totalpoints = new HashMap<String, Double>();
    private static Map<String, Double> totalkills = new HashMap<String, Double>();
    private static Map<String, Double> totaldeaths = new HashMap<String, Double>();
    private static Map<String, Double> sessionpoints = new HashMap<String, Double>();
    private static Map<String, Double> sessionkills = new HashMap<String, Double>();
    private static Map<String, Double> sessiondeaths = new HashMap<String, Double>();
    private static Map<String, Map<String, Double>> classpoints = new HashMap<String, Map<String, Double>>();
    private static Map<String, Map<String, Double>> classkills = new HashMap<String, Map<String, Double>>();
    private static Map<String, Map<String, Double>> classdeaths = new HashMap<String, Map<String, Double>>();
    private static FileConfiguration SConfig = null;
    private static File SConfigFile = null;

    public void LoadStats() {
        for(String p : getSConfig().getStringList("Players")) {
            addDeaths(p, getSConfig().getDouble(p + ".td"));
            addKills(p, getSConfig().getDouble(p + ".tk"));
            addPoints(p, getSConfig().getDouble(p + ".tp"));
        }
    }

    public static double getTotalPoints(String player) {
        if(totalpoints.get(player) != null) {
            return totalpoints.get(player);
        } else {
            return 0;
        }
    }

    public static double getTotalKills(String player) {
        if(totalkills.get(player) != null) {
            return totalkills.get(player);
        } else {
            return 0;
        }
    }

    public static double getTotalDeaths(String player) {
        if(totaldeaths.get(player) != null) {
            return totaldeaths.get(player);
        } else {
            return 0;
        }
    }

    public static double getSesPoints(String player) {
        if(sessionpoints.get(player) != null) {
            return sessionpoints.get(player);
        } else {
            return 0;
        }
    }

    public static double getSesKills(String player) {
        if(sessionkills.get(player) != null) {
            return sessionkills.get(player);
        } else {
            return 0;
        }
    }

    public static double getSesDeaths(String player) {
        if(sessiondeaths.get(player) != null) {
            return sessiondeaths.get(player);
        } else {
            return 0;
        }
    }

    public static double getClassPoints(String player) {
        Player p = Bukkit.getPlayer(player);
        String cls = classes.getUserClass(p);
        if(classpoints.get(cls) != null && classpoints.get(cls).get(pmethods.playerGame(p)) != null) {
            return classpoints.get(cls).get(pmethods.playerGame(p));
        } else {
            return 0;
        }
    }

    public static double getClassKills(String player) {
        Player p = Bukkit.getPlayer(player);
        String cls = classes.getUserClass(p);
        if(classkills.get(cls) != null && classkills.get(cls).get(pmethods.playerGame(p)) != null) {
            return classkills.get(cls).get(pmethods.playerGame(p));
        } else {
            return 0;
        }
    }

    public static double getClassDeaths(String player) {
        Player p = Bukkit.getPlayer(player);
        String cls = classes.getUserClass(p);
        if(classdeaths.get(cls) != null && classdeaths.get(cls).get(pmethods.playerGame(p)) != null) {
            return classdeaths.get(cls).get(pmethods.playerGame(p));
        } else {
            return 0;
        }
    }

    public static String topClass(String map) {
        double initp = 0;
        String tcls = "ERROR";
        for(String cls : classes.getAllClasses()) {
            if(classpoints.get(cls) != null && classpoints.get(cls).get(map) != null) {
                if(classpoints.get(cls).get(map) > initp) {
                    initp = classpoints.get(cls).get(map);
                    tcls = cls;
                }
            }
        }
        return tcls;
    }

    public static Set<String> getPlayers() {
        return totalpoints.keySet();
    }

    public static void addPoints(String player, double points) {
        if(totalpoints.get(player) != null) {
            double p1 = totalpoints.get(player);
            double p2 = p1 + points;
            totalpoints.put(player, p2);
        } else {
            totalpoints.put(player, points);
        }
        if(sessionpoints.get(player) != null) {
            double p1 = sessionpoints.get(player);
            double p2 = p1 + points;
            sessionpoints.put(player, p2);
        } else {
            sessionpoints.put(player, points);
        }
        Player p = Bukkit.getPlayer(player);
        if(p != null && classes.isClassed(p)) {
            String cls = classes.getUserClass(p);
            if(classpoints.get(cls) != null && classpoints.get(cls).get(pmethods.playerGame(p)) != null) {
                double p1 = classpoints.get(cls).get(pmethods.playerGame(p));
                double p2 = p1 + points;
                classpoints.get(cls).put(pmethods.playerGame(p), p2);
            } else {
                classpoints.put(cls, new HashMap<String, Double>());
                classpoints.get(cls).put(pmethods.playerGame(p), points);
            }
        }
    }

    public static void addKills(String player, double points) {
        if(totalkills.get(player) != null) {
            double p1 = totalkills.get(player);
            double p2 = p1 + points;
            totalkills.put(player, p2);
        } else {
            totalkills.put(player, points);
        }
        if(sessionkills.get(player) != null) {
            double p1 = sessionkills.get(player);
            double p2 = p1 + points;
            sessionkills.put(player, p2);
        } else {
            sessionkills.put(player, points);
        }
        Player p = Bukkit.getPlayer(player);
        if(p != null && classes.isClassed(p)) {
            String cls = classes.getUserClass(p);
            if(classkills.get(cls) != null && classkills.get(cls).get(pmethods.playerGame(p)) != null) {
                double p1 = classkills.get(cls).get(pmethods.playerGame(p));
                double p2 = p1 + points;
                classkills.get(cls).put(pmethods.playerGame(p), p2);
            } else {
                classkills.put(cls, new HashMap<String, Double>());
                classkills.get(cls).put(pmethods.playerGame(p), points);
            }
        }
    }

    public static void addDeaths(String player, double points) {
        if(totaldeaths.get(player) != null) {
            double p1 = totaldeaths.get(player);
            double p2 = p1 + points;
            totaldeaths.put(player, p2);
        } else {
            totaldeaths.put(player, points);
        }
        if(sessiondeaths.get(player) != null) {
            double p1 = sessiondeaths.get(player);
            double p2 = p1 + points;
            sessiondeaths.put(player, p2);
        } else {
            sessiondeaths.put(player, points);
        }
        Player p = Bukkit.getPlayer(player);
        if(p != null && classes.isClassed(p)) {
            String cls = classes.getUserClass(p);
            if(classdeaths.get(cls) != null && classdeaths.get(cls).get(pmethods.playerGame(p)) != null) {
                double p1 = classdeaths.get(cls).get(pmethods.playerGame(p));
                double p2 = p1 + points;
                classdeaths.get(cls).put(pmethods.playerGame(p), p2);
            } else {
                classdeaths.put(cls, new HashMap<String, Double>());
                classdeaths.get(cls).put(pmethods.playerGame(p), points);
            }
        }
    }

    public static void clearClassStats(String map) {
        for(String cls : classes.getAllClasses()) {
            if(classpoints.get(cls) != null) {
                classpoints.get(cls).remove(map);
            }
            if(classdeaths.get(cls) != null) {
                classdeaths.get(cls).remove(map);
            }
            if(classkills.get(cls) != null) {
                classkills.get(cls).remove(map);
            }
        }
    }

    public static void setPoints(String player, double p) {
        sessionpoints.put(player, p);
    }

    public static void setKills(String player, double p) {
        sessionkills.put(player, p);
    }

    public static void setDeaths(String player, double p) {
        sessiondeaths.put(player, p);
    }

    public static void clear() {
        sessiondeaths.clear();
        sessionkills.clear();
        sessionpoints.clear();
    }

    public static void removeSplayer(String player) {
        sessiondeaths.remove(player);
        sessionkills.remove(player);
        sessionpoints.remove(player);
    }

    public static void removeTplayer(String player) {
        totaldeaths.remove(player);
        totalkills.remove(player);
        totalpoints.remove(player);
    }

    public static void removeSplayerPoints(String player) {
        sessionpoints.remove(player);
    }

    public static void removeSplayerKills(String player) {
        sessionkills.remove(player);
    }

    public static void removeSplayerDeaths(String player) {
        sessiondeaths.remove(player);
    }

    public static void removeTplayerPoints(String player) {
        totalpoints.remove(player);
    }

    public static void removeTplayerKills(String player) {
        totalkills.remove(player);
    }

    public static void removeTplayerDeaths(String player) {
        totaldeaths.remove(player);
    }

    public static String topScorePlayer() {
        double initp = 0;
        String user = "ERROR";
        for(String p : totalpoints.keySet()) {
            if(totalpoints.get(p) > initp) {
                initp = totalpoints.get(p);
                user = p;
            }
        }
        return user;
    }

    public static String topKillsPlayer() {
        double initp = 0;
        String user = "ERROR";
        for(String p : totalkills.keySet()) {
            if(totalkills.get(p) > initp) {
                initp = totalkills.get(p);
                user = p;
            }
        }
        return user;
    }

    public static String topDeathsPlayer() {
        double initp = 0;
        String user = "ERROR";
        for(String p : totaldeaths.keySet()) {
            if(totaldeaths.get(p) > initp) {
                initp = totaldeaths.get(p);
                user = p;
            }
        }
        return user;
    }

    public static double topScore() {
        return totalpoints.get(topScorePlayer());
    }

    public static double topKills() {
        return totalkills.get(topKillsPlayer());
    }

    public static double topDeaths() {
        return totaldeaths.get(topDeathsPlayer());
    }

    public void saveSConfig() {
        if(SConfig == null || SConfigFile == null) {
            return;
        }
        try {
            getSConfig().save(SConfigFile);
        } catch(IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + SConfigFile, ex);
        }
    }

    public FileConfiguration getSConfig() {
        if(SConfig == null) {
            reloadSConfig();
        }
        return SConfig;
    }

    public void reloadSConfig() {
        if(SConfigFile == null) {
            SConfigFile = new File(plugin.getDataFolder(), "stats.yml");
        }
        SConfig = YamlConfiguration.loadConfiguration(SConfigFile);
        InputStream defConfigStream = plugin.getResource("stats.yml");
        if(defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            SConfig.setDefaults(defConfig);
        }
    }

    public void SaveStats() {
        List<String> players = new ArrayList<String>();
        for(String p : stats.getPlayers()) {
            players.add(p);
            getSConfig().set(p + ".td", stats.getTotalDeaths(p));
            getSConfig().set(p + ".tk", stats.getTotalKills(p));
            getSConfig().set(p + ".tp", stats.getTotalPoints(p));
        }
        getSConfig().set("Players", players);
        saveSConfig();
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
