/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bjornke.zombiesurvival;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class revive implements Listener {

    Plugin plugin;
    public Map<Sign, String> timedSigns = new ConcurrentHashMap<Sign, String>();

    public void counterTask() {
        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            public void run() {
                for(Iterator<Sign> it = timedSigns.keySet().iterator(); it.hasNext();) {
                    Sign s = it.next();
                    String line4 = s.getLine(3);
                    String chars = line4.substring(2);
                    int time = Integer.parseInt(chars);
                    time--;
                    if(time > 0) {
                        if(time > 15) {
                            s.setLine(3, "§a" + Integer.toString(time));
                            s.update();
                        } else if(time > 5) {
                            s.setLine(3, "§e" + Integer.toString(time));
                            s.update();
                        } else {
                            s.setLine(3, "§c" + Integer.toString(time));
                            s.update();
                        }
                    } else {
                        Block b = s.getBlock();
                        b.setTypeId(0);
                        it.remove();
                    }
                }
            }

        }, 20, 20);
    }

    public void removeSign(String name) {
        for(Iterator<Sign> it = timedSigns.keySet().iterator(); it.hasNext();) {
            Sign s = it.next();
            if(timedSigns.get(s).matches(name)) {
                Block b = s.getBlock();
                b.setTypeId(0);
                it.remove();
            }
        }
    }

    public void createRevive(Player p) {
        Block b = p.getLocation().getBlock();
        Location middle = p.getLocation();
        if(b.getTypeId() != 0) {
            for(int y = middle.getBlockY() - 2; y <= middle.getBlockY() + 2; y++) {
                for(int x = middle.getBlockX() - 6; x <= middle.getBlockX() + 6; x++) {
                    for(int z = middle.getBlockZ() - 6; z <= middle.getBlockZ() + 6; z++) {
                        Location temp = new Location(middle.getWorld(), x, middle.getBlockY(), z);
                        if(temp.getBlock().getTypeId() == 0 && temp.getBlock().getRelative(BlockFace.DOWN).getTypeId() != 0) {
                            b = temp.getBlock();
                            break;
                        }
                    }
                }
            }
        }
        if(b.getTypeId() == 0) {
            b.getWorld().strikeLightningEffect(b.getLocation());
            b.setType(Material.SIGN_POST);
            Sign s = (Sign)b.getState();
            s.setLine(0, "BREAK TO REVIVE");
            s.setLine(1, "§1" + p.getName());
            s.setLine(2, "§4TIME LEFT");
            s.setLine(3, "§a30");
            s.update();
            timedSigns.put(s, p.getName());
        } else {
            plugin.getLogger().warning("Could not create revive sign for: " + p.getName());
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onSignBreak(BlockBreakEvent e) {
        Block b = e.getBlock();
        Player player = e.getPlayer();
        if(b.getState() instanceof Sign) {
            Sign s = (Sign)b.getState();
            if(timedSigns.containsKey(s)) {
                Player p = Bukkit.getPlayer(timedSigns.get(s));
                if(p != null && pmethods.inGame(p) && !p.isDead()) {
                    b.setTypeId(0);
                    games.setPcount(pmethods.playerGame(p), games.getPcount(pmethods.playerGame(p)) + 1);
                    main.dead.remove(p.getName());
                    spectate.spectators.remove(p.getName());
                    p.teleport(s.getLocation());
                    p.setAllowFlight(false);
                    p.setFlying(false);
                    p.setGameMode(GameMode.SURVIVAL);
                    p.setHealth(20);
                    p.setFoodLevel(20);
                    utilities.unhidePlayer(p);
                    timedSigns.remove(s);
                    player.sendMessage(ChatColor.GREEN + "You revived: " + ChatColor.GRAY + p.getName());
                    p.sendMessage(ChatColor.GREEN + "You were revived by: " + ChatColor.GRAY + player.getName());
                    e.setCancelled(true);
                } else if(p != null || !pmethods.inGame(p)) {
                    b.setTypeId(0);
                    timedSigns.remove(s);
                    e.setCancelled(true);
                }
            }
        }
    }

    public void Destroy() {
        try {
            this.finalize();
        } catch(Throwable e) {
            plugin.getLogger().warning("Failed to destroy class");
        }
    }

}
