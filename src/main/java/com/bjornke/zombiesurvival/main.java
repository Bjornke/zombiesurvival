package com.bjornke.zombiesurvival;

import java.io.*;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.minecraft.server.v1_6_R2.EntityHuman;
import net.minecraft.server.v1_6_R2.EntityLiving;
import net.minecraft.server.v1_6_R2.EntityWolf;
import net.minecraft.server.v1_6_R2.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_6_R2.PathfinderGoalSelector;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_6_R2.entity.CraftWolf;
import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Brandon Ragland "Bjornke"
 */
public class main extends JavaPlugin implements Runnable, Listener {

    public main instance;
    public Random random = new Random();
    public spawns spawn = new spawns(this);
    public awards awards = new awards(this);
    public barricades bar = new barricades();
    public language lang = new language(this);
    public fPerk perk = new fPerk();
    public potions pot = new potions(this);
    public smartGames sg = new smartGames();
    public doors door = new doors(this);
    public revive revive = new revive();
    public classes classes = new classes();
    public stats stats = new stats();
    public Map<String, Integer> secondkills = new HashMap<String, Integer>(); //for perks
    public int onlinep = 0;
    public Map<String, Integer> justleftgame = new HashMap<String, Integer>();
    public Map<String, Boolean> wolfwave = new HashMap<String, Boolean>();
    //Easy Create stuff
    public Map<String, Integer> easycreate = new HashMap<String, Integer>();
    public Map<String, String> ecname = new HashMap<String, String>();
    public Map<String, Integer> ecpcount = new HashMap<String, Integer>();
    public Map<String, Integer> ecwcount = new HashMap<String, Integer>();
    public Map<String, Double> eczcount = new HashMap<String, Double>();
    public Map<String, door> playerdoorlink = new HashMap<String, door>();
    //end easy create stuff
    public List<String> twomintimer = new ArrayList<String>();
    public int startpcount; //Number of players need to start game
    public Map<String, Integer> commandwave = new HashMap<String, Integer>(); //wave for adding spawns/doors via zsa-command
    public Map<String, String> commandMap = new HashMap<String, String>(); //For adding stuff to the correct Map instance
    public Map<String, Integer> perkcount = new HashMap<String, Integer>(); //Countdown for perks
    public String VERSION = "R-3.6";
    public boolean outofdate = false;
    public boolean antigreif = false; //Internal anti-greif
    public boolean itemsatjoin = false; //Give items when /join
    public boolean infectmat = false;
    public boolean spectateallow = false;
    public boolean perpnight = false;
    public boolean emptyaccount = true;
    public boolean forcespawn = false;
    public boolean respawn = true;
    public boolean allhurt = true;
    public boolean forceclear = false;
    public boolean invsave = true;
    public double seekspeed = 0.23;
    public double fastseekspeed = 0.40;
    public boolean wolfs = true;
    public boolean healnewwave = true;
    public boolean nvchecker = false;
    public boolean resetpointsdeath = true;
    public boolean jm = true;
    public boolean smartw = true;
    public String joinmessage = "Welcome to the server!";
    public List<String> joincommand = new ArrayList<String>();
    public List<String> leavecommand = new ArrayList<String>();
    public int cooldown = 120;
    public int vspoke = 0;
    public int runnerchance = 10;
    public int skellywavechance = 0;
    public int wait = 20;
    public int doorfindradius = 6;
    public int leavetimer = 120;
    public int maxpoints = 20;
    public double deathloss = 0.00;
    public double diffmulti = 0.1;
    public double damagemulti = 1.00;
    public Map<String, Integer> cooldowncount = new HashMap<String, Integer>();
    public Map<String, Location> lightloc = new HashMap<String, Location>(10); //Where should the starting wave lightning strike?
    public List<ItemStack> joinitems = new ArrayList<ItemStack>(5);
    public List<ItemStack> joinarmor = new ArrayList<ItemStack>(5);
    public Map<ItemStack, Double> drops = new HashMap<ItemStack, Double>(5);
    public List<ItemStack> boxitems = new ArrayList<ItemStack>(10);
    public Map<Location, String> roundFire = new HashMap<Location, String>();   //Every 10th round what blocks should be set on fire?
    public Map<String, Map<Block, BlockState>> changedBlocks = new HashMap<String, Map<Block, BlockState>>(); //To reset map if game in progress
    public Map<String, Map<Block, BlockState>> placedBlocks = new HashMap<String, Map<Block, BlockState>>(); //To reset map if game in progress
    public List<Integer> blockbreak = new ArrayList<Integer>(); //What blocks can be broken during games
    public List<Integer> blockplace = new ArrayList<Integer>(); //What blocks can be placed during games
    public Map<Location, String> specialblocks = new HashMap<Location, String>(); //Special wool blocks that do cool stuff!
    public Map<String, pData> playerdata = new HashMap<String, pData>();
    public Map<Integer, String> zombies = new HashMap<Integer, String>(200); //Zombie Entities
    public Map<Integer, String> fastzombies = new HashMap<Integer, String>(5);
    public Map<Integer, Map<String, Integer>> zombiescore = new HashMap<Integer, Map<String, Integer>>();
    public static Map<String, String> dead = new ConcurrentHashMap<String, String>(); //Dead players
    public Map<String, Location> deathPoints = new HashMap<String, Location>(); //Death points
    public Map<String, Boolean> perkend = new HashMap<String, Boolean>();
    public static Map<String, String> Maps = new HashMap<String, String>(10);
    public Set<Location> Signs = new HashSet<Location>();
    public Set<String> voted = new HashSet<String>();
    public Map<String, Integer> wavemax = new HashMap<String, Integer>();
    //Adding Stuff to the lists and configuration :)
    public Map<String, Integer> add = new HashMap<String, Integer>(2); //0-lobby 1-spectate 2-lightloc 3-spawns 4-fire 5-doors 6-build
    public Map<String, Integer> remove = new HashMap<String, Integer>(2);
    int task; //Useless junk for the run()
    int task2;
    public static Economy econ = null;
    public static Chat chat = null;
    public boolean points = true;
    private FileConfiguration customConfig = null;
    private File customConfigFile = null;
    private static Field targetSelector;
    Locale bLocale = new Locale("en", "US");

    @Override
    public void onEnable() {
        instance = this;
        try {
            saveDefaultConfig();
            saveResource("language.yml", false);
            saveResource("awards.yml", false);
            saveResource("classes.yml", false);
            registerEvents();
            lang.LoadLanguage();
            stats.plugin = this;
            classes.plugin = this;
            door.plugin = this;
            spawn.plugin = this;
            pot.plugin = this;
            revive.plugin = this;
            bar.plugin = this;
            classes.LoadClasses();
            stats.LoadStats();
            awards.LoadAwards();
            task = scheduleTask(this, 20L, 20L);
            if(setupEconomy()) {
                points = false;
                getLogger().info("Using Vault for Economy Access!");
            }
            reloadPlayers();
            QuickUpdate();
            checkMobs();
            AsynchTasks();
            synchTasks();
            revive.counterTask();
            perpNight();
            dealDamage();
            LoadConfig();
            if(!points) {
                points = getConfig().getBoolean("force-points");
            }
            getLogger().info("ZombieSurvival Enabled!");
            try {
                metrics metrics = new metrics(this);
                metrics.start();
            } catch(IOException e) {
                getLogger().info("Metrics failed to start");
            }
            try {
                targetSelector = EntityLiving.class.getDeclaredField("targetSelector");
                if(targetSelector == null) {
                    getLogger().warning("Unable to use Java Reflection to gain access to wolf behavior. No wolves allowed.");
                    wolfs = false;
                } else {
                    targetSelector.setAccessible(true);
                }
            } catch(Exception e) {
                getLogger().warning("Unable to use Java Reflection to gain access to wolf behavior. No wolves allowed.");
                wolfs = false;
            }
            if(nvchecker) {
                Version();
            }
        } catch(Exception e) {
            e.printStackTrace();
            getLogger().severe("ZombieSurvival failed to start correctly! Disabling!");
            getLogger().severe(e.toString());
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {
        door.saveDoor();
        spawn.saveSpawn();
        bar.saveBar();
        saveSigns();
        for(String map : Maps.keySet()) {
            GamesOver(map, true);
        }
        getServer().getScheduler().cancelTasks(this);
        awards.Destroy();
        bar.Destroy();
        door.Destroy();
        pot.Destroy();
        spawn.Destroy();
        revive.Destroy();
        spawn.Destroy();
        getLogger().info("ZombieSurvival Disabled!");
    }

    private void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(this, this);
        pm.registerEvents(new signs(), this);
        pm.registerEvents(perk, this);
        pm.registerEvents(new spectate(), this);
        pm.registerEvents(revive, this);
        pm.registerEvents(new donator(), this);
    }

    public void LoadConfig() {
        //list of stuff
        door.loadDoor();
        spawn.loadSpawn();
        bar.loadBar();
        pot.LoadPotions();
        List<String> worldnames = new ArrayList<String>();
        for(World world2 : Bukkit.getWorlds()) {
            worldnames.add(world2.getName());
        }
        for(String w : worldnames) {
            for(String s : getCustomConfig().getStringList(w + ".maps")) {
                Maps.put(s, w);
            }
        }
        for(String str2 : getCustomConfig().getStringList("signs")) {
            if(parseToLoc(str2) != null) {
                Signs.add(parseToLoc(str2));
            }
        }
        saveSigns();
        for(String str : Maps.keySet()) {
            for(String str2 : getCustomConfig().getStringList(Maps.get(str) + "." + str + ".roundfire")) {
                roundFire.put(parseToLoc(str2), str);
            }
            for(String str2 : getCustomConfig().getStringList(Maps.get(str) + "." + str + ".specialblocks")) {
                specialblocks.put(parseToLoc(str2), str);
            }
            deathPoints.put(str, parseToLoc(getCustomConfig().getString(Maps.get(str) + "." + str + ".waiting")));
            spawn.lobbies.put(str, parseToLoc(getCustomConfig().getString(Maps.get(str) + "." + str + ".lobby")));
            spawn.spectate.put(str, parseToLoc(getCustomConfig().getString(Maps.get(str) + "." + str + ".spectate")));
            lightloc.put(str, parseToLoc(getCustomConfig().getString(Maps.get(str) + "." + str + ".lightning")));
            perk.addLoc(str, parseToLoc(getCustomConfig().getString(Maps.get(str) + "." + str + ".perk")));
            games.setMaxZombies(str, getCustomConfig().getInt(Maps.get(str) + "." + str + ".maxzombies"));
            games.setMaxPlayers(str, getCustomConfig().getInt(Maps.get(str) + "." + str + ".maxplayers"));
            games.setMaxWaves(str, getCustomConfig().getInt(Maps.get(str) + "." + str + ".maxwaves"));
            games.setState(str, 1);
            games.setPcount(str, 0);
            perk.setPerk(str, 0);
            perkcount.put(str, 0);
            changedBlocks.put(str, new HashMap<Block, BlockState>());
            placedBlocks.put(str, new HashMap<Block, BlockState>());
            perkend.put(str, true);
            games.setWave(str, 0);
            games.setZcount(str, 0);
        }
        for(String it : getConfig().getStringList("join-items")) {
            try {
                String[] line = it.split(":");
                int itemid = Integer.parseInt(line[0]);
                short itemdamage = 0;
                if(line.length > 1) {
                    itemdamage = Short.parseShort(line[1]);
                }
                ItemStack item = new ItemStack(itemid, 1, itemdamage);
                joinitems.add(item);
            } catch(Exception e) {
                getLogger().warning("Could not load item config: " + it);
            }
        }
        for(String it : getConfig().getStringList("armor-items")) {
            try {
                String[] line = it.split(":");
                int itemid = Integer.parseInt(line[0]);
                short itemdamage = 0;
                if(line.length > 1) {
                    itemdamage = Short.parseShort(line[1]);
                }
                ItemStack item = new ItemStack(itemid, 1, itemdamage);
                joinarmor.add(item);
            } catch(Exception e) {
                getLogger().warning("Could not load item config: " + it);
            }
        }
        for(String it : getConfig().getStringList("mysterybox-items")) {
            try {
                String[] line = it.split(":");
                int itemid = Integer.parseInt(line[0]);
                short itemdamage = 0;
                if(line.length > 1) {
                    itemdamage = Short.parseShort(line[1]);
                }
                ItemStack item = new ItemStack(itemid, 1, itemdamage);
                boxitems.add(item);
            } catch(Exception e) {
                getLogger().warning("Could not load item config: " + it);
            }
        }
        for(String it : getConfig().getStringList("drop-items")) {
            try {
                String[] line = it.split(":");
                int itemid = Integer.parseInt(line[0]);
                short itemdamage = 0;
                double chance = 0.5;
                if(line.length > 1) {
                    itemdamage = Short.parseShort(line[1]);
                }
                if(line.length > 2) {
                    chance = Double.parseDouble(line[2]);
                }
                ItemStack item = new ItemStack(itemid, 1, itemdamage);
                drops.put(item, chance);
            } catch(Exception e) {
                getLogger().warning("Could not load item config: " + it);
            }
        }
        blockbreak = getConfig().getIntegerList("allowbreak");
        blockplace = getConfig().getIntegerList("allowplace");
        startpcount = getConfig().getInt("start-player-count");
        antigreif = getConfig().getBoolean("auto-anti-grief");
        itemsatjoin = getConfig().getBoolean("items-at-join");
        joinmessage = getConfig().getString("auto-join-message");
        cooldown = getConfig().getInt("auto-cooldown");
        deathloss = getConfig().getDouble("death-loss-percent");
        diffmulti = getConfig().getDouble("health-multi");
        damagemulti = getConfig().getDouble("damage-multi");
        infectmat = getConfig().getBoolean("infect-mat");
        spectateallow = getConfig().getBoolean("allow-spectate");
        wait = getConfig().getInt("wave-wait") * 20;
        perpnight = getConfig().getBoolean("perp-night");
        runnerchance = getConfig().getInt("runner-chance");
        pot.effectchance = getConfig().getInt("effect-chance");
        emptyaccount = getConfig().getBoolean("empty-account");
        forcespawn = getConfig().getBoolean("force-spawn");
        seekspeed = getConfig().getDouble("seek-speed");
        fastseekspeed = getConfig().getDouble("fast-seek-speed");
        pot.bitelength = getConfig().getInt("bite-effect-length") * 20;
        doorfindradius = getConfig().getInt("buy-door-find-radius");
        respawn = getConfig().getBoolean("death-non-human-respawn");
        allhurt = getConfig().getBoolean("all-hurt");
        leavetimer = getConfig().getInt("leave-timer");
        forceclear = getConfig().getBoolean("inventory-clear-join");
        invsave = getConfig().getBoolean("inventory-save");
        healnewwave = getConfig().getBoolean("heal-player-new-wave");
        skellywavechance = getConfig().getInt("wolf-wave-chance");
        joincommand = getConfig().getStringList("join-commands");
        leavecommand = getConfig().getStringList("leave-commands");
        nvchecker = getConfig().getBoolean("new-version-checking");
        maxpoints = getConfig().getInt("max-points-per-kill");
        resetpointsdeath = getConfig().getBoolean("reset-points-on-death");
        jm = getConfig().getBoolean("join-message");
        smartw = getConfig().getBoolean("use-smart-waves");
        String v = getConfig().getString("version");
        /*if(!v.equals(VERSION) || v == null) {
         saveResource("config.yml", true);
         Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.RED + "CONFIG HAS BEEN REPLACED BECAUSE IT WAS OUT OF DATE!");
         }
         */
        saveCustomConfig();
    }

    @EventHandler
    public void CommandListener(PlayerCommandPreprocessEvent e) {
        String message = e.getMessage().substring(1);
        String[] command = message.split(" ");
        if(joincommand.contains(command[0])) {
            if(command.length > 1) {
                e.getPlayer().performCommand("bsapj " + command[1]);
            } else {
                e.getPlayer().performCommand("bsapj ");
            }
            e.setCancelled(true);
        } else if(leavecommand.contains(command[0])) {
            e.getPlayer().performCommand("bsapl");
            e.setCancelled(true);
        }
    }

    public void reload() {
        for(String str : Maps.keySet()) {
            GamesOver(str, true);
        }
        lightloc.clear();
        blockbreak.clear();
        blockplace.clear();
        roundFire.clear();
        door.doors.clear();
        add.clear();
        remove.clear();
        changedBlocks.clear();
        placedBlocks.clear();
        zombies.clear();
        perkcount.clear();
        dead.clear();
        reloadConfig();
        reloadCustomConfig();
        LoadConfig();
        reloadPlayers();
    }

    public void run() {
        for(String map : Maps.keySet()) {
            World world = Bukkit.getWorld(Maps.get(map));
            int s = games.getState(map);
            if(s == 2 && games.getZcount(map) < wavemax.get(map)) {
                LivingEntity lent;
                Entity ent;
                if(wolfwave.get(map)) {
                    ent = world.spawnEntity(spawn.spawn(map, games.getWave(map)), EntityType.WOLF);
                    Wolf w = (Wolf)ent;
                    w.setAngry(true);
                } else {
                    ent = world.spawnEntity(spawn.spawn(map, games.getWave(map)), EntityType.ZOMBIE);
                }
                lent = (LivingEntity)ent;
                if(diffmulti != 0) {
                    int health = (int)(games.getWave(map) * diffmulti);
                    lent.setMaxHealth(3 + health);
                    lent.setHealth(3 + health);
                    lent.setRemoveWhenFarAway(false);
                }
                if(runnerchance != 0) {
                    int go = random.nextInt(runnerchance);
                    if(go == 1) {
                        fastzombies.put(ent.getEntityId(), map);
                    }
                }
                zombies.put(ent.getEntityId(), map);
                games.setZcount(map, games.getZcount(map) + 1);
            }
            if(s == 2 && secondkills.get(map) > games.getPcount(map) && perk.getPerk(map) == 0) {
                perk.callPerk(map);
                perkend.put(map, false);
            }
            secondkills.put(map, 0);
            if(perk.getPerk(map) > 0 && perkcount.get(map) < 60) {
                perkcount.put(map, perkcount.get(map) + 1);
            } else {
                if(!perkend.get(map) && !perk.isDropped) {
                    for(String str : pmethods.playersInMap(map)) {
                        Player p = Bukkit.getPlayer(str);
                        if(p != null) {
                            p.sendMessage(lang.strings.get(9));
                        }
                    }
                    perkend.put(map, true);
                }
                perk.setPerk(map, 0);
                perkcount.put(map, 0);
            }
        }
    }
    //Game Starting Equipment

    public void Games(String map, Boolean force) {
        World world = Bukkit.getWorld(Maps.get(map));
        if(games.getPcount(map) >= startpcount || force) {
            games.setState(map, 2);
            games.setWave(map, 1);
            games.setZslayed(map, 0);
            games.setZcount(map, 0);
            secondkills.put(map, 0);
            wolfwave.put(map, false);
            if(smartw) {
                wavemax.put(map, sg.smartWaveCount(map));
            } else {
                maxfinder(map);
            }
            String health = Integer.toString(4 + (int)(games.getWave(map) * diffmulti));
            String zombmax = Integer.toString(wavemax.get(map));
            if(lightloc.get(map) != null) {
                world.strikeLightningEffect(lightloc.get(map));
            }
            getLogger().info(lang.strings.get(0) + ": " + map);
            for(String mp : pmethods.playersInMap(map)) {
                Player p = Bukkit.getPlayer(mp);
                if(p != null) {
                    p.teleport(spawn.lobbies.get(map));
                    p.setAllowFlight(false);
                    p.setFlying(false);
                    p.setGameMode(GameMode.SURVIVAL);
                    p.setHealth(20);
                    p.setFoodLevel(20);
                    dead.remove(p.getName());
                    joinItems(p);
                    classes.setupPlayer(p);
                    p.sendMessage(lang.strings.get(2));
                    p.sendMessage(lang.strings.get(3));
                    p.sendMessage(ChatColor.DARK_RED + zombmax + " " + lang.strings.get(4) + " " + ChatColor.DARK_RED + health + " " + lang.strings.get(5));
                }
            }
        } else {
            for(String mp : pmethods.playersInMap(map)) {
                Player p = Bukkit.getPlayer(mp);
                if(p != null) {
                    p.sendMessage(lang.strings.get(6));
                }
            }
        }
    }
    //Joining Existsing Game

    public void placeInGame(Player p, String map, boolean wave) {
        getLogger().info(map + ": from PlaceInGame()");
        utilities.unhidePlayer(p);
        stats.setPoints(p.getName(), 0);
        p.teleport(spawn.lobbies.get(map));
        p.setAllowFlight(false);
        p.setFlying(false);
        p.setGameMode(GameMode.SURVIVAL);
        p.setHealth(20);
        p.setFoodLevel(20);
        joinItems(p);
        classes.setupPlayer(p);
        if(!wave) {
            p.sendMessage(lang.strings.get(2));
        }
    }
    //Game Ending Equiptment

    public void GamesOver(String map, Boolean force) {
        World world = Bukkit.getWorld(Maps.get(map));
        if(pmethods.onlinepcount(map) < 1 || force) {
            resetBlocks(map); //Will have to come back to this
            utilities.clearDrops(map);
            resetDoors(map);
            stats.SaveStats();
            games.setState(map, 1);
            games.setWave(map, 0);
            games.setPcount(map, 0);
            stats.clearClassStats(map);
            bar.resetBars(map);
            vspoke = 0;
            wavemax.put(map, null);
            getLogger().info(lang.strings.get(1) + ": " + map);
            List<LivingEntity> templist = getLivingEnts(world);
            for(LivingEntity went : templist) {
                for(Iterator<Integer> it = zombies.keySet().iterator(); it.hasNext();) {
                    int id = it.next();
                    if(zombies.get(id).equalsIgnoreCase(map) && went.getEntityId() == id) {
                        if(went.isValid()) {
                            went.remove();
                        }
                        it.remove();
                    }
                }
            }
            for(String mp : pmethods.playersInMap(map)) {
                games.removePlayerMap(mp);
                stats.setPoints(mp, 0);
                stats.setDeaths(mp, 0);
                stats.removeSplayerKills(mp);
                dead.remove(mp);
                spectate.spectators.remove(mp);
                Player p = Bukkit.getPlayer(mp);
                revive.removeSign(mp);
                if(p != null) {
                    p.teleport(playerdata.get(mp).location);
                    p.setFlying(false);
                    p.setGameMode(playerdata.get(mp).gamemode);
                    p.setHealth(playerdata.get(mp).health);
                    p.setFoodLevel(playerdata.get(mp).food);
                    if(invsave) {
                        p.getInventory().setContents(playerdata.get(mp).inventory);
                        p.getInventory().setArmorContents(playerdata.get(mp).armor);
                    }
                    playerdata.remove(mp);
                    utilities.unhidePlayer(p);
                    p.setDisplayName(p.getName());
                    p.sendMessage(lang.strings.get(7));
                    if(emptyaccount && !points) {
                        double removem = econ.getBalance(mp);
                        econ.withdrawPlayer(p.getName(), removem);
                    }
                }
            }
        }
    }
    //Parsing Equiptment for Loads and Saves on the Config!

    public Location parseToLoc(String str) throws NumberFormatException {
        if(str == null) {
            return null;
        }
        String[] strs = str.split(" ");
        double xl = Double.parseDouble(strs[0]);
        double yl = Double.parseDouble(strs[1]);
        double zl = Double.parseDouble(strs[2]);
        World worldl = Bukkit.getServer().getWorld(strs[3]);
        Location parsedLoc = new Location(worldl, xl, yl, zl);
        return parsedLoc;
    }

    public String parseToStr(Location parseloc) {
        return String.format(bLocale, "%.2f %.2f %.2f %s", parseloc.getX(), parseloc.getY(), parseloc.getZ(), parseloc.getWorld().getName());
    }

    public int scheduleTask(Runnable runnable, long initial, long delay) {
        return Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, runnable, initial, delay);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("zsdebug")) {
            if(sender instanceof Player) {
                Player player = (Player)sender;
                zsDebug(1, player);
                return true;
            } else {
                zsDebug(0, null);
                return true;
            }
        }
        if(sender instanceof Player) {
            Player player = (Player)sender;
            //0-lobby 1-spectate 2-lightloc 3-spawns 4-fire 5-doors 6-special 7-deathPoints
            if(cmd.getName().equalsIgnoreCase("zsa-spawn")) {
                if(args.length != 2) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                add.put(player.getName(), 3);
                commandMap.put(player.getName(), args[0]);
                commandwave.put(player.getName(), Integer.parseInt(args[1]));
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(10));
                spawn.showSpawns(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zsr-spawn")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 3);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(10));
                spawn.showSpawns(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zs-start")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                Bukkit.broadcastMessage(ChatColor.AQUA + "Admin has started the game!");
                Games(args[0], true);
            } else if(cmd.getName().equalsIgnoreCase("zs-end")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                GamesOver(args[0], true);
                player.sendMessage(ChatColor.GOLD + "You have ended the games early!");
            } else if(cmd.getName().equalsIgnoreCase("zs-create")) {
                if(args.length != 4) {
                    return false;
                }
                try {
                    commandMap.put(player.getName(), args[0]);
                    add.put(player.getName(), 0);
                    remove.remove(player.getName());
                    getCustomConfig().set(player.getWorld().getName() + "." + args[0] + ".maxzombies", (int)(Double.parseDouble(args[1]) * 100));
                    getCustomConfig().set(player.getWorld().getName() + "." + args[0] + ".maxplayers", Integer.parseInt(args[2]));
                    getCustomConfig().set(player.getWorld().getName() + "." + args[0] + ".maxwaves", Integer.parseInt(args[3]));
                    games.setMaxZombies(args[0], (int)(Double.parseDouble(args[1]) * 100));
                    games.setMaxPlayers(args[0], Integer.parseInt(args[2]));
                    games.setMaxWaves(args[0], Integer.parseInt(args[3]));
                    games.setPcount(args[0], 0);
                    games.setState(args[0], 1);
                    Maps.put(args[0], player.getWorld().getName());
                    perk.setPerk(args[0], 0);
                    perkcount.put(args[0], 0);
                    perkend.put(args[0], false);
                    games.setWave(args[0], 0);
                    changedBlocks.put(args[0], new HashMap<Block, BlockState>());
                    placedBlocks.put(args[0], new HashMap<Block, BlockState>());
                    List<String> gamemaps = new ArrayList<String>();
                    for(String s : Maps.keySet()) {
                        if(Maps.get(s).equalsIgnoreCase(player.getWorld().getName())) {
                            gamemaps.add(s);
                        }
                    }
                    getCustomConfig().set(Maps.get(args[0]) + ".maps", gamemaps);
                    saveCustomConfig();
                } catch(UnknownFormatConversionException ex) {
                    player.sendMessage(ChatColor.RED + "Try again, bad arguments!");
                    return false;
                }
                player.sendMessage(lang.strings.get(12));
            } else if(cmd.getName().equalsIgnoreCase("zs-remove")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                getCustomConfig().set(Maps.get(args[0]) + ".maps", Maps);
                getCustomConfig().set(Maps.get(args[0]) + "." + args[0], null);
                Maps.remove(args[0]);
                saveCustomConfig();
                reload();
                player.sendMessage(lang.strings.get(59) + args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zsa-fire")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 4);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(13));
            } else if(cmd.getName().equalsIgnoreCase("zsr-fire")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 4);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(14));
            } else if(cmd.getName().equalsIgnoreCase("zsa-spectate")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 1);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(15));
            } else if(cmd.getName().equalsIgnoreCase("zsr-spectate")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 1);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(16));
            } else if(cmd.getName().equalsIgnoreCase("zsa-waiting")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 7);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(17));
            } else if(cmd.getName().equalsIgnoreCase("zsr-waiting")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 7);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(18));
            } else if(cmd.getName().equalsIgnoreCase("zsa-lightning")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 2);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(19));
            } else if(cmd.getName().equalsIgnoreCase("zsr-lightning")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 2);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(20));
            } else if(cmd.getName().equalsIgnoreCase("zsa-door")) {
                if(args.length != 2) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 5);
                commandwave.put(player.getName(), Integer.parseInt(args[1]));
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(21));
                door.showDoors(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zsr-door")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 5);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(22));
                door.showDoors(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zs-reload")) {
                reload();
                player.sendMessage("Reloaded ZombieSurvival!");
            } else if(cmd.getName().equalsIgnoreCase("zsa-special")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 6);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(23));
            } else if(cmd.getName().equalsIgnoreCase("zsr-special")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 6);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(24));
            } else if(cmd.getName().equalsIgnoreCase("zsa-bar")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 8);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(55));
                bar.showBarricades(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zsr-bar")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                remove.put(player.getName(), 8);
                add.remove(player.getName());
                player.sendMessage(lang.strings.get(56));
                bar.showBarricades(args[0]);
            } else if(cmd.getName().equalsIgnoreCase("zsa-perk")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                add.put(player.getName(), 9);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(57));
            } else if(cmd.getName().equalsIgnoreCase("zsr-perk")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                commandMap.put(player.getName(), args[0]);
                removefPerk(args[0]);
                player.sendMessage(lang.strings.get(58));
            } else if(cmd.getName().equalsIgnoreCase("zs-version")) {
                Version();
                if(outofdate) {
                    player.sendMessage(ChatColor.GOLD + "Version: " + VERSION);
                    player.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.RED + "OUT OF DATE! UPDATE AVAILABLE");
                } else {
                    player.sendMessage(ChatColor.GREEN + "Version: " + VERSION);
                }
            } else if(cmd.getName().equalsIgnoreCase("zstp")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                player.teleport(spawn.lobbies.get(args[0]));
            } else if(cmd.getName().equalsIgnoreCase("zs-c")) {
                easycreate.put(player.getName(), 0);
                player.sendMessage(lang.strings.get(60));
            } else if(cmd.getName().equalsIgnoreCase("zs-doorlink")) {
                if(args.length != 1) {
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                add.put(player.getName(), 10);
                commandMap.put(player.getName(), args[0]);
                remove.remove(player.getName());
                player.sendMessage(lang.strings.get(70));
                door.showDoors(args[0]);
            }
            if(cmd.getName().equalsIgnoreCase("stats")) {
                if(args.length < 1) {
                    if(pmethods.inGame(player)) {
                        info(player, player);
                    }
                }
                if(args.length > 0) {
                    Player p = Bukkit.getPlayer(args[0]);
                    if(p != null && pmethods.inGame(p)) {
                        info(p, player);
                    } else {
                        player.sendMessage(lang.strings.get(61));
                    }
                }
                player.sendMessage("Number of players online: " + Integer.toString(onlinep));
            } else if(cmd.getName().equalsIgnoreCase("whisper")) {
                Player other = (Bukkit.getServer().getPlayer(args[0]));
                if(other == null) {
                    return false;
                }
                String send = (" MESSAGE:");
                for(int i = 1; i < args.length; i++) {
                    send += " " + args[i];
                }
                other.sendMessage("From: " + player.getName() + send);
                player.sendMessage("Sent To: " + other.getName() + "Message:" + send);
            } else if(cmd.getName().equalsIgnoreCase("zshelp")) {
                player.sendMessage("Commands: stats, whisper, join, leave, zshelp");
                if(player.isOp()) {
                    player.performCommand("help ZombieSurvival");
                }
            } else if(cmd.getName().equalsIgnoreCase("bsapj")) {
                if(args.length < 1) {
                    String message = (lang.strings.get(64) + Maps.keySet().toString());
                    player.sendMessage(ChatColor.BLUE + message);
                    return false;
                }
                if(!Maps.containsKey(args[0])) {
                    player.sendMessage(lang.strings.get(8));
                    return false;
                }
                if(justleftgame.containsKey(player.getName())) {
                    player.sendMessage(lang.strings.get(62) + Integer.toString(leavetimer - justleftgame.get(player.getName())) + lang.strings.get(63));
                    return true;
                }
                if(pmethods.numberInMap(args[0]) < games.getMaxPlayers(args[0]) && (!(pmethods.inGame(player))) && !dead.containsKey(player.getName()) && stats.getSesPoints(player.getName()) >= 0) {
                    if(jm) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "[ZombieSurvival] " + ChatColor.GREEN + player.getName() + " just joined " + args[0] + "!");
                    }
                    games.setPlayerMap(player.getName(), args[0]);
                    stats.setPoints(player.getName(), 0);
                    stats.setKills(player.getName(), 0);
                    games.setPcount(args[0], (games.getPcount(args[0]) + 1));
                    pData pData = new pData();
                    pData.name = player.getName();
                    pData.inventory = player.getInventory().getContents();
                    pData.armor = player.getInventory().getArmorContents();
                    pData.food = player.getFoodLevel();
                    pData.gamemode = player.getGameMode();
                    pData.health = (int)player.getHealth();
                    pData.location = player.getLocation();
                    playerdata.put(player.getName(), pData);
                    if(games.getState(args[0]) < 2) {
                        Games(args[0], false);
                    } else if(games.getState(args[0]) > 1) {
                        placeInGame(player, args[0], false);
                    }
                    player.sendMessage(lang.strings.get(54));
                } else {
                    player.sendMessage(lang.strings.get(65));
                }
            } else if(cmd.getName().equalsIgnoreCase("bsapl") && pmethods.inGame(player)) {
                String map = pmethods.playerGame(player);
                justleftgame.put(player.getName(), 0);
                if(!dead.containsKey(player.getName())) {
                    int tempcount = games.getPcount(map);
                    games.setPcount(map, (tempcount - 1));
                }
                player.teleport(playerdata.get(player.getName()).location);
                player.setGameMode(playerdata.get(player.getName()).gamemode);
                player.setFlying(false);
                spectate.spectators.remove(player.getName());
                player.sendMessage(lang.strings.get(66));
                games.removePlayerMap(player.getName());
                stats.setPoints(player.getName(), 0);
                stats.removeSplayerKills(player.getName());
                dead.remove(player.getName());
                utilities.unhidePlayer(player);
                if(emptyaccount && !points) {
                    double removem = econ.getBalance(player.getName());
                    econ.withdrawPlayer(player.getName(), removem);
                }
                if(invsave) {
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(null);
                    player.getInventory().setContents(playerdata.get(player.getName()).inventory);
                }
                playerdata.remove(player.getName());
                GamesOver(map, false);
                String name = player.getName();
                player.setDisplayName(name);
            }
        } else {
            sender.sendMessage("Must be player!");
            return false;
        }

        return true;
    }

    public void info(Player p, Player s) {
        String str3 = rPlayers(pmethods.playerGame(p));
        s.sendMessage(ChatColor.GREEN + "Current Wave: " + ChatColor.DARK_RED + Integer.toString(games.getWave(pmethods.playerGame(p))));
        s.sendMessage(ChatColor.GREEN + "Kills: " + ChatColor.DARK_RED + Double.toString(stats.getSesKills(p.getName())));
        s.sendMessage(ChatColor.GREEN + "All-Time Kills: " + ChatColor.DARK_RED + Double.toString(stats.getTotalKills(p.getName())));
        if(classes.isClassed(p)) {
            s.sendMessage(ChatColor.GREEN + "Class Kills: " + ChatColor.DARK_RED + Double.toString(stats.getClassKills(p.getName())));
        }
        if(!points) {
            String score = String.format("%.1f", econ.getBalance(p.getName()));
            s.sendMessage(ChatColor.GREEN + "Balance: " + ChatColor.DARK_RED + score);
            s.sendMessage(ChatColor.GREEN + "All-Time Balance: " + ChatColor.DARK_RED + Double.toString(stats.getTotalPoints(p.getName())));
            if(classes.isClassed(p)) {
                s.sendMessage(ChatColor.GREEN + "Class Balance: " + ChatColor.DARK_RED + Double.toString(stats.getClassPoints(p.getName())));
            }
        } else {
            s.sendMessage(ChatColor.GREEN + "Points: " + ChatColor.DARK_RED + Double.toString(stats.getSesPoints(p.getName())));
            s.sendMessage(ChatColor.GREEN + "All-Time Points: " + ChatColor.DARK_RED + Double.toString(stats.getTotalPoints(p.getName())));
            if(classes.isClassed(p)) {
                s.sendMessage(ChatColor.GREEN + "Class Points: " + ChatColor.DARK_RED + Double.toString(stats.getClassPoints(p.getName())));
            }
        }
        s.sendMessage(ChatColor.GREEN + "Deaths: " + ChatColor.DARK_RED + Double.toString(stats.getSesDeaths(p.getName())));
        s.sendMessage(ChatColor.GREEN + "All-Time Deaths: " + ChatColor.DARK_RED + Double.toString(stats.getTotalDeaths(p.getName())));
        if(classes.isClassed(p)) {
            s.sendMessage(ChatColor.GREEN + "Class Deaths: " + ChatColor.DARK_RED + Double.toString(stats.getClassDeaths(p.getName())));
        }
        s.sendMessage(ChatColor.GREEN + "Remaining Players: " + str3);
    }

    public String rPlayers(String map) {
        String string = (ChatColor.DARK_RED + "");
        for(String p : pmethods.playersInMap(map)) {
            if(!dead.containsKey(p)) {
                string = (string.concat(p) + ChatColor.GRAY + ", " + ChatColor.DARK_RED);
            }
        }
        return string;
    }

    public void NewWave(final String map) { //When to increment wave
        games.setState(map, 3);
        games.setWave(map, games.getWave(map) + 1);
        bar.resetBars(map);
        if(skellywavechance != 0) {
            int go = random.nextInt(skellywavechance);
            if(go == 1 && games.getWave(map) != 1 && wolfs) {
                wolfwave.put(map, true);
            } else {
                wolfwave.put(map, false);
            }
        }
        if(Integer.toString(games.getWave(map)).endsWith("0")) {
            for(Location blg : roundFire.keySet()) {
                if(roundFire.get(blg).equalsIgnoreCase(map)) {
                    blg.getBlock().getRelative(BlockFace.UP).setType(Material.FIRE);
                }
            }
        }
        if(games.getWave(map) > games.getMaxWaves(map)) {
            GamesOver(map, true);
            return;
        }
        String health = "20";
        if(diffmulti != 0) {
            int h = (int)(games.getWave(map) * diffmulti);
            health = Integer.toString(3 + h);
        }
        String zombmax;
        if(smartw) {
            zombmax = Integer.toString(sg.smartWaveCount(map));
        } else {
            zombmax = utilities.annouceMax(map);
        }
        if(games.getWave(map) != 1) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                @Override
                public void run() {
                    if(smartw) {
                        wavemax.put(map, sg.smartWaveCount(map));
                    } else {
                        maxfinder(map);
                    }
                    games.setZcount(map, 0);
                    games.setZslayed(map, 0);
                    games.setState(map, 2);
                }

            }, wait);
        }
        Boolean doorsopen = false;
        String tcls = stats.topClass(map);
        for(String player : pmethods.playersInMap(map)) {
            Player p = Bukkit.getPlayer(player);
            if(p != null) {
                p.sendMessage(ChatColor.GREEN + "WAVE: " + ChatColor.DARK_RED + Integer.toString(games.getWave(map)) + ChatColor.GRAY + " starts in " + ChatColor.DARK_RED + Integer.toString(wait / 20) + ChatColor.GRAY + " seconds!");
                if(wolfwave.get(map)) {
                    p.sendMessage(ChatColor.GRAY + "The zombies have called in the " + ChatColor.DARK_RED + "wolves!" + ChatColor.GRAY + " Prepare for the slaughter!");
                    p.sendMessage(ChatColor.DARK_RED + zombmax + ChatColor.GRAY + " wolves with " + ChatColor.DARK_RED + health + ChatColor.GRAY + " health!");
                } else {
                    p.sendMessage(ChatColor.DARK_RED + zombmax + ChatColor.GRAY + " zombies with " + ChatColor.DARK_RED + health + ChatColor.GRAY + " health!");
                }
                if(!tcls.equalsIgnoreCase("ERROR")) {
                    p.sendMessage(ChatColor.DARK_RED + "Wave Best Class: " + ChatColor.GRAY + tcls);
                }
            }
        }
        for(Iterator<String> it = dead.keySet().iterator(); it.hasNext();) {
            String player = it.next();
            if(dead.get(player).equalsIgnoreCase(map)) {
                Player p = Bukkit.getPlayer(player);
                if(p != null) {
                    placeInGame(p, dead.get(player), true);
                    games.setPcount(map, games.getPcount(map) + 1);
                    it.remove();
                }
            }
        }
        for(Iterator<String> it = spectate.spectators.keySet().iterator(); it.hasNext();) {
            String name = it.next();
            if(spectate.spectators.get(name).matches(map)) {
                Player p = Bukkit.getPlayer(name);
                utilities.unhidePlayer(p);
                it.remove();
            }
        }
        for(door d : door.doorWave(map, games.getWave(map))) {
            Block dblock = d.location.getBlock();
            dblock.setType(Material.AIR);
            doorsopen = true;
        }
        for(String player : pmethods.playersInMap(map)) {
            Player p = Bukkit.getPlayer(player);
            if(p != null) {
                if(healnewwave) {
                    p.setHealth(20);
                    p.setFoodLevel(20);
                }
                if(doorsopen) {
                    p.sendMessage(lang.strings.get(67));
                }
            }
        }
    }
    //Resetting Equipment

    public void resetBlocks(String map) {
        List<Location> fire = new ArrayList<Location>();
        if(!roundFire.isEmpty() && roundFire != null) {
            for(Location str : roundFire.keySet()) {
                if(roundFire.get(str).equalsIgnoreCase(map)) {
                    fire.add(str);
                }
            }
            for(Location b : fire) {
                b.getBlock().getRelative(BlockFace.UP).setType(Material.AIR);
            }
            fire.clear();
        }
        if(!changedBlocks.isEmpty() && changedBlocks.get(map) != null) {
            for(Block b : changedBlocks.get(map).keySet()) {
                BlockState bstate = changedBlocks.get(map).get(b);
                b.setTypeIdAndData(bstate.getTypeId(), bstate.getRawData(), false);
            }
            changedBlocks.get(map).clear();
        }
        if(!placedBlocks.isEmpty() && placedBlocks.get(map) != null) {
            for(Block p : placedBlocks.get(map).keySet()) {
                p.setType(Material.AIR);
            }
            placedBlocks.get(map).clear();
        }
    }
    //Adding to Config
    //@todo add functionality to Lightning and Spectator

    public void addLobby(Location lobbyloc, String map) {
        lobbyloc.add(0.5, 1, 0.5);
        spawn.lobbies.put(map, lobbyloc);
        String savelobbyloc = parseToStr(lobbyloc);
        getCustomConfig().set(Maps.get(map) + "." + map + ".lobby", savelobbyloc);
        saveCustomConfig();
    }

    public void removeLobby(Location lobbyloc, String map) {
        lobbyloc.add(0.5, 1, 0.5);
        if(spawn.lobbies.containsKey(map) && spawn.lobbies.get(map).equals(lobbyloc)) {
            spawn.lobbies.remove(map);
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".lobby", null);
        saveCustomConfig();
    }

    public void addFire(Location fireblock, String map) {
        roundFire.put(fireblock, map);
        List<String> adeath = new ArrayList<String>();
        for(Location str : roundFire.keySet()) {
            if(roundFire.get(str).equalsIgnoreCase(map)) {
                adeath.add(parseToStr(str));
            }
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".roundfire", adeath);
        saveCustomConfig();
    }

    public void removeFire(Location fireblock, String map) {
        roundFire.remove(fireblock);
        List<String> adeath = new ArrayList<String>();
        for(Location str : roundFire.keySet()) {
            if(roundFire.get(str).equalsIgnoreCase(map)) {
                adeath.add(parseToStr(str));
            }
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".roundfire", adeath);
        saveCustomConfig();
    }

    public void addSpecial(Location specialblock, String map) {
        specialblocks.put(specialblock, map);
        List<String> adeath = new ArrayList<String>();
        for(Location str : specialblocks.keySet()) {
            if(specialblocks.get(str).equalsIgnoreCase(map)) {
                adeath.add(parseToStr(str));
            }
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".specialblocks", adeath);
        saveCustomConfig();
    }

    public void removeSpecial(Location specialblock, String map) {
        specialblocks.remove(specialblock);
        List<String> adeath = new ArrayList<String>();
        for(Location str : specialblocks.keySet()) {
            if(specialblocks.get(str).equalsIgnoreCase(map)) {
                adeath.add(parseToStr(str));
            }
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".specialblocks", adeath);
        saveCustomConfig();
    }

    public void addBar(Block b, String map) {
        bar.addBar(b, map);
    }

    public void removeBar(Block b, String map) {
        bar.removeBar(bar.findBar(b.getX(), b.getY(), b.getZ()));
    }

    public void addfPerk(Location specialblock, String map) {
        specialblock.add(0.5, 0.1, 0.5);
        perk.addLoc(map, specialblock);
        String str = parseToStr(specialblock);
        getCustomConfig().set(Maps.get(map) + "." + map + ".perk", str);
        saveCustomConfig();
    }

    public void removefPerk(String map) {
        perk.removeLoc(map);
        getCustomConfig().set(Maps.get(map) + "." + map + ".perk", "");
        saveCustomConfig();
    }

    public void addSpawn(Block b, int wavenumber, String map) {
        spawn.addSpawn(b, map, wavenumber);
    }

    public void removeSpawn(Block b, String map) {
        spawn.removeSpawn(spawn.findSpawn(b.getX() + 0.5, b.getY(), b.getZ() + 0.5));
    }

    public void addDoor(Block b, int wavenumber, String map) {
        door.addDoor(b, map, wavenumber);
    }

    public void removeDoor(Location b) {
        door.removeDoor(door.findDoor(b.getX(), b.getY(), b.getZ()));
    }

    public void addSpec(Location specloc, String map) {
        specloc.add(0.5, 1, 0.5);
        spawn.spectate.put(map, specloc);
        String savelobbyloc = parseToStr(specloc);
        getCustomConfig().set(Maps.get(map) + "." + map + ".spectate", savelobbyloc);
        saveCustomConfig();
    }

    public void removeSpec(Location specloc, String map) { //@todo look into why we need to add to location
        specloc.add(0.5, 1, 0.5);
        if(spawn.spectate.containsKey(map) && spawn.spectate.get(map).equals(specloc)) {
            spawn.spectate.remove(map);
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".spectate", null);
        saveCustomConfig();
    }

    public void addLight(Location lightloca, String map) {
        lightloca.add(0.5, 1, 0.5);
        lightloc.put(map, lightloca);
        String savelobbyloc = parseToStr(lightloca);
        getCustomConfig().set(Maps.get(map) + "." + map + ".lightning", savelobbyloc);
        saveCustomConfig();
    }

    public void removeLight(Location lightloca, String map) { //@todo look into ..too, same as removeSpec
        lightloca.add(0.5, 1, 0.5);
        if(lightloc.containsKey(map) && lightloc.get(map).equals(lightloca)) {
            lightloc.remove(map);
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".lightning", null);
        saveCustomConfig();
    }

    public void addDeath(Location deathloc, String map) {
        deathloc.add(0.5, 1, 0.5);
        deathPoints.put(map, deathloc);
        String savelobbyloc = parseToStr(deathloc);
        getCustomConfig().set(Maps.get(map) + "." + map + ".waiting", savelobbyloc);
        saveCustomConfig();
    }

    public void removeDeath(Location deathloc, String map) {
        deathloc.add(0.5, 1, 0.5);
        if(deathPoints.containsKey(map) && deathPoints.get(map).equals(deathloc)) {
            deathPoints.remove(map);
        }
        getCustomConfig().set(Maps.get(map) + "." + map + ".waiting", null);
        saveCustomConfig();
    }
    //Handlers

    @EventHandler
    public void onOpAction(PlayerInteractEvent event) {
        int OPadd = -1;
        int OPremove = -1;
        Block block = event.getClickedBlock();
        Player player = event.getPlayer();
        if(player.isOp() || player.hasPermission("zs.edit")) {
            //0-lobby 1-spectate 2-lightloc 3-spawns 4-fire 5-doors 6-build 7-deathPoints 8-barricades 9-perk 10-door->spawn
            if(add.containsKey(player.getName())) {
                OPadd = add.get(player.getName());
            }
            if(remove.containsKey(player.getName())) {
                OPremove = remove.get(player.getName());
            }
            switch(OPadd) {
                case 0:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addLobby(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(25));
                    } else if(easycreate.get(player.getName()) != null && easycreate.get(player.getName()) == 4) {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(43));
                        player.performCommand("zsa-spawn " + commandMap + " 1");
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 1:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addSpec(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(26));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 2:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addLight(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(27));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 3:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addSpawn(block, commandwave.get(player.getName()), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(28));
                    } else if(easycreate.get(player.getName()) != null && easycreate.get(player.getName()) == 4) {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(44));
                        easycreate.remove(player.getName());
                    } else {
                        spawn.hideSpawns(commandMap.get(player.getName()));
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 4:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addFire(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(29));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 5:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addDoor(block, commandwave.get(player.getName()), commandMap.get(player.getName()));
                        if(block.getTypeId() == 64) {
                            smartDoors(block, commandMap.get(player.getName()), commandwave.get(player.getName()));
                        }
                        player.sendMessage(lang.strings.get(30));
                    } else {
                        door.hideDoors(commandMap.get(player.getName()));
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 6:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        if(block.getTypeId() == 35) {
                            addSpecial(block.getLocation(), commandMap.get(player.getName()));
                            player.sendMessage(lang.strings.get(31));
                        } else {
                            player.sendMessage(lang.strings.get(42));
                        }
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 7:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addDeath(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(32));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 8:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addBar(block, commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(45));
                    } else {
                        bar.hideBarricades(commandMap.get(player.getName()));
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 9:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        addfPerk(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(46));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 10:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        if(playerdoorlink.get(player.getName()) == null) {
                            door d = door.findDoor(block.getX(), block.getY(), block.getZ());
                            if(d != null && d.map.matches(commandMap.get(player.getName()))) {
                                playerdoorlink.put(player.getName(), d);
                                door.hideDoors(commandMap.get(player.getName()));
                                spawn.showSpawns(commandMap.get(player.getName()));
                            } else {
                                player.sendMessage(lang.strings.get(71));
                                break;
                            }
                        } else {
                            spawn s = spawn.findSpawn(block.getX() + 0.5, block.getY(), block.getZ() + 0.5);
                            if(s != null && s.map.matches(commandMap.get(player.getName()))) {
                                door d = playerdoorlink.get(player.getName());
                                d.spawns.add(s);
                                spawn.hideSpawn(s);
                                player.sendMessage(lang.strings.get(72));
                            } else {
                                player.sendMessage(lang.strings.get(73));
                                break;
                            }
                        }
                    } else {
                        add.remove(player.getName());
                        playerdoorlink.remove(player.getName());
                        door.hideDoors(commandMap.get(player.getName()));
                        spawn.hideSpawns(commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
            }
            switch(OPremove) {
                case 0:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeLobby(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(33));
                    } else {
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 1:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeSpec(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(34));
                    } else {
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 2:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeLight(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(35));
                    } else {
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 3:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeSpawn(block, commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(36));
                    } else {
                        spawn.hideSpawns(commandMap.get(player.getName()));
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 4:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeFire(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(37));
                    } else {
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 5:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeDoor(block.getLocation());
                        player.sendMessage(lang.strings.get(38));
                    } else {
                        door.hideDoors(commandMap.get(player.getName()));
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 6:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        if(block.getTypeId() == 35) {
                            removeSpecial(block.getLocation(), commandMap.get(player.getName()));
                            player.sendMessage(lang.strings.get(39));
                        } else {
                            player.sendMessage(lang.strings.get(42));
                        }
                    } else {
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 7:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeDeath(block.getLocation(), commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(40));
                    } else {
                        add.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 8:
                    if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        removeBar(block, commandMap.get(player.getName()));
                        player.sendMessage(lang.strings.get(48));
                    } else {
                        bar.hideBarricades(commandMap.get(player.getName()));
                        remove.remove(player.getName());
                        player.sendMessage(lang.strings.get(41));
                    }
                    break;
                case 10:
                    break;
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK && player.hasPermission("zs.signs")) {
            Block block = event.getClickedBlock();
            if(block.getState() instanceof Sign) {
                Sign sign = (Sign)block.getState();
                String[] lines = sign.getLines();
                if(pmethods.inGame(player)) {
                    if(games.getState(pmethods.playerGame(player)) > 1) {
                        if(lines.length < 1) {
                            return;
                        }
                        if(lines[0].equalsIgnoreCase("§9zombie") && !(lines[1].contains("heal")) && !(lines[1].contains("leave")) && !(lines[1].contains("Repair"))) {
                            String[] temp = lines[3].split(":");
                            String[] temp2 = lines[2].split("-");
                            int cost = 0;
                            int item = 0;
                            int amount = 1;
                            short damage = 0;
                            try {
                                amount = Integer.parseInt(temp2[0]);
                                cost = Integer.parseInt(temp2[1]);
                                damage = Short.parseShort(temp[1]);
                                item = Integer.parseInt(temp[0]);
                            } catch(Exception e) {
                                player.sendMessage(lang.strings.get(48));
                            }
                            if(!points && econ.getBalance(player.getName()) >= cost) {
                                player.getInventory().addItem(new ItemStack(item, amount, damage));
                                player.updateInventory();
                                econ.withdrawPlayer(player.getName(), cost);
                                player.sendMessage(lang.strings.get(52) + Integer.toString(cost) + ChatColor.DARK_RED + " dollars!");
                            } else if(stats.getSesPoints(player.getName()) >= cost) {
                                player.getInventory().addItem(new ItemStack(item, amount, damage));
                                player.updateInventory();
                                stats.addPoints(player.getName(), -cost);
                                String name = player.getName();
                                player.setDisplayName("[" + Double.toString(stats.getSesPoints(player.getName())) + "]" + name);
                                player.sendMessage(lang.strings.get(52) + Integer.toString(cost) + ChatColor.DARK_RED + " points!");
                            } else {
                                player.sendMessage(lang.strings.get(49));
                            }
                        }
                        if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].contains("heal")) {
                            int cost2heal = 50;
                            try {
                                cost2heal = Integer.parseInt(lines[2]);
                            } catch(Exception e) {
                                player.sendMessage(lang.strings.get(48));
                            }
                            if(!points && econ.getBalance(player.getName()) >= cost2heal) {
                                econ.withdrawPlayer(player.getName(), cost2heal);
                                player.setHealth(20);
                                player.setFoodLevel(20);
                                player.sendMessage(lang.strings.get(53) + Integer.toString(cost2heal) + ChatColor.DARK_RED + " dollars");
                            } else if(stats.getSesPoints(player.getName()) >= cost2heal) {
                                player.setHealth(20);
                                player.setFoodLevel(20);
                                stats.addPoints(player.getName(), -cost2heal);
                                String name = player.getName();
                                player.setDisplayName("[" + Double.toString(stats.getSesPoints(player.getName())) + "]" + name);
                                player.sendMessage(lang.strings.get(53) + Integer.toString(cost2heal) + ChatColor.DARK_RED + " points");
                            } else {
                                player.sendMessage(lang.strings.get(49));
                            }
                        }
                        if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].contains("Repair")) {
                            if(bar.healBars(sign.getLocation(), pmethods.playerGame(player))) {
                                if(!points) {
                                    econ.depositPlayer(player.getName(), 1);
                                    player.sendMessage(lang.strings.get(75));
                                } else {
                                    stats.addPoints(player.getName(), 1);
                                    player.sendMessage(lang.strings.get(50));
                                }
                            } else {
                                player.sendMessage(lang.strings.get(74));
                            }
                        }
                        if(lines[0].equalsIgnoreCase("§9zombie") && lines[1].contains("leave")) {
                            player.performCommand("bsapl");
                        }
                        if(lines[0].equalsIgnoreCase("§9zombie door")) {
                            int cost2open = 100;
                            try {
                                cost2open = Integer.parseInt(lines[2]);
                            } catch(Exception e) {
                                player.sendMessage(lang.strings.get(48));
                            }
                            if(!points && econ.getBalance(player.getName()) >= cost2open) {
                                try {
                                    econ.withdrawPlayer(player.getName(), cost2open);
                                    openDoors(sign.getLocation(), lines[1]);
                                    player.sendMessage(lang.strings.get(51) + Integer.toString(cost2open) + ChatColor.DARK_RED + " dollars!");
                                } catch(Exception e) {
                                    e.printStackTrace();
                                    player.sendMessage(lang.strings.get(48));
                                }
                            } else if(stats.getSesPoints(player.getName()) >= cost2open) {
                                try {
                                    stats.addPoints(player.getName(), -cost2open);
                                    String name = player.getName();
                                    player.setDisplayName("[" + Double.toString(stats.getSesPoints(player.getName())) + "]" + name);
                                    openDoors(sign.getLocation(), lines[1]);
                                    player.sendMessage(lang.strings.get(51) + Integer.toString(cost2open) + ChatColor.DARK_RED + " points!");
                                } catch(Exception e) {
                                    e.printStackTrace();
                                    player.sendMessage(lang.strings.get(48));
                                }
                            } else {
                                player.sendMessage(lang.strings.get(49));
                            }
                        }
                    }
                }
                if(lines[0].equalsIgnoreCase("§dzombie stats") && !lines[1].contains("zombies")) {
                    if(justleftgame.containsKey(player.getName())) {
                        player.sendMessage(lang.strings.get(62) + Integer.toString(leavetimer - justleftgame.get(player.getName())) + lang.strings.get(63));
                        return;
                    }
                    player.performCommand("bsapj " + lines[1]);
                }
            }
            if(block.getState() instanceof Chest) {
                Chest chest = (Chest)block.getState();
                if(pmethods.inGame(player)) {
                    if(!mysteryBox(block.getLocation(), chest, player)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        List<Location> spblocks = new ArrayList<Location>();
        Player player = event.getPlayer();
        if(pmethods.inGame(player)) {
            if(games.getState(pmethods.playerGame(player)) > 1) {
                Location location = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() - 1, player.getLocation().getZ());
                Block block = location.getBlock();
                for(Location str : specialblocks.keySet()) {
                    if(specialblocks.get(str).equalsIgnoreCase(pmethods.playerGame(player))) {
                        spblocks.add(str);
                    }
                }
                if(block.getTypeId() == 35 && spblocks.contains(block.getLocation())) {
                    Wool wool = new Wool(block.getType(), block.getData());
                    if(wool.getColor() == DyeColor.YELLOW) {
                        player.setVelocity(player.getLocation().getDirection().multiply(2));
                    }
                    if(wool.getColor() == DyeColor.BLACK && !player.isDead()) {
                        player.setHealth(0);
                    }
                    if(wool.getColor() == DyeColor.GREEN) {
                        player.setVelocity(player.getLocation().getDirection().multiply(0.5).setY(0));
                    }
                    if(wool.getColor() == DyeColor.BLUE) {
                        player.setVelocity(player.getLocation().getDirection().setY(1));
                    }
                    if(wool.getColor() == DyeColor.ORANGE && player.getFireTicks() == 0) {
                        player.setFireTicks(200);
                    }
                    if(wool.getColor() == DyeColor.LIGHT_BLUE) {
                        player.setVelocity(player.getLocation().getDirection().setY(2));
                    }
                    if(wool.getColor() == DyeColor.SILVER) {
                        utilities.hidePlayer(player);
                    }
                    if(wool.getColor() == DyeColor.GRAY) {
                        utilities.unhidePlayer(player);
                    }
                }
            }
        }
        spblocks.clear();
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if(pmethods.inGame(player)) {
            String map = pmethods.playerGame(player);
            if(games.getState(map) > 1) {
                if(blockbreak.contains(block.getTypeId()) && !changedBlocks.get(map).containsKey(block)) {
                    changedBlocks.get(map).put(block, block.getState());
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Not allowed!");
                }
            }
            if(games.getState(map) == 1 && antigreif && !player.isOp()) {
                event.setCancelled(true);
            }
        }
        if(Signs.contains(block.getLocation()) && !pmethods.inGame(player) && !event.isCancelled() && player.hasPermission("zs.edit")) {
            Signs.remove(block.getLocation());
            saveSigns();
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if(pmethods.inGame(player)) {
            String map = pmethods.playerGame(player);
            if(games.getState(map) > 1) {
                if(blockplace.contains(block.getTypeId())) {
                    placedBlocks.get(map).put(block, block.getState());
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "Not allowed!");
                }
            }
            if(games.getState(map) == 1 && antigreif && !player.isOp()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if(pmethods.inGame(player)) {
            String map = pmethods.playerGame(player);
            if(games.getState(map) > 1) {
                if(blockbreak.contains(block.getTypeId()) && !changedBlocks.get(map).containsKey(block)) {
                    changedBlocks.get(map).put(block, block.getState());
                } else {
                    event.setCancelled(true);
                }
            }
            if(games.getState(map) == 1 && antigreif && !player.isOp()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamagePvP(EntityDamageByEntityEvent event) {
        Entity ent = event.getEntity();
        Entity dEnt = event.getDamager();
        int entid = ent.getEntityId();
        int dEntid = dEnt.getEntityId();
        Player player = null;
        Player damager;
        if(ent instanceof Player && dEnt instanceof Player) {
            player = (Player)ent;
            damager = (Player)dEnt;
            String map = pmethods.playerGame(player);
            if(pmethods.inGame(player)) {
                if(games.getState(map) > 1) {
                    if(pmethods.playerGame(player).equalsIgnoreCase(pmethods.playerGame(damager))) {
                        if(dead.containsKey(damager.getName())) {
                            event.setCancelled(true);
                        }
                        if(points) {
                            double dscore = stats.getSesPoints(damager.getName());
                            double dnewscore = (dscore - event.getDamage());
                            stats.addPoints(damager.getName(), -event.getDamage());
                            damager.sendMessage(lang.strings.get(68));
                            String dname = damager.getName();
                            damager.setDisplayName("[" + dnewscore + "]" + dname);
                        } else {
                            econ.withdrawPlayer(damager.getName(), (double)event.getDamage());
                            damager.sendMessage(ChatColor.RED + "You have been deducted " + Integer.toString((int)event.getDamage()) + " for attacking another survivor!");
                        }
                    }
                }
                if(games.getState(map) == 1 && antigreif) {
                    event.setCancelled(true);
                }
            }
        }
        if(zombies.containsKey(entid) && !(dEnt instanceof Player) && !(dEnt instanceof Projectile)) {
            event.setCancelled(true);
        }
        if(zombies.containsKey(entid) && (dEnt instanceof Player || dEnt instanceof Arrow)) {
            if(dEnt instanceof Arrow) {
                Arrow arrow = (Arrow)dEnt;
                if(arrow.getShooter() instanceof Player) {
                    player = (Player)arrow.getShooter();
                }
            } else {
                player = (Player)dEnt;
            }
            if(pmethods.inGame(player) && zombies.containsKey(entid)) {
                String map = pmethods.playerGame(player);
                if(zombies.get(entid).equalsIgnoreCase(map) && !dead.containsKey(player.getName())) {
                    if(zombiescore.get(entid) == null) {
                        Map<String, Integer> temp = new HashMap<String, Integer>();
                        temp.put(player.getName(), (int)event.getDamage());
                        zombiescore.put(entid, temp);
                    } else if(zombiescore.get(entid).get(player.getName()) == null) {
                        zombiescore.get(entid).put(player.getName(), (int)event.getDamage());
                    } else {
                        int olddamg = zombiescore.get(entid).get(player.getName());
                        int damg = (olddamg + (int)event.getDamage());
                        zombiescore.get(entid).put(player.getName(), damg);
                    }
                    if(zombiescore.get(entid).get(player.getName()) != null && zombiescore.get(entid).get(player.getName()) > maxpoints) {
                        zombiescore.get(entid).put(player.getName(), maxpoints);
                    }
                }
                switch(perk.getPerk(map)) {
                    case 2:
                        LivingEntity lent = (LivingEntity)ent;
                        lent.setHealth(0);
                        break;
                    case 3:
                        event.getEntity().setFireTicks(60);
                        break;
                    case 5:
                        ent.setVelocity(player.getLocation().getDirection().multiply(3));
                }
            }
        }
        if(ent instanceof Player && zombies.containsKey(dEntid)) {
            player = (Player)ent;
            if(pmethods.inGame(player)) {
                String map = pmethods.playerGame(player);
                if(perk.getPerk(map) == 1) {
                    event.setCancelled(true);
                }
                if(damagemulti != 0) {
                    event.setDamage(2 + (int)(games.getWave(map) * damagemulti));
                }
                pot.checkForBite(player);
            }
        }
    }

    @EventHandler
    public void onZombieDamage(EntityDamageEvent event) {
        Entity ent = event.getEntity();
        if(!zombies.isEmpty() && zombies.get(ent.getEntityId()) != null) {
            if(zombies.containsKey(ent.getEntityId())) {
                if(games.getState(zombies.get(ent.getEntityId())) > 1) {
                    if(!(event instanceof EntityDamageByEntityEvent) && !allhurt) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onZombieDeath(EntityDeathEvent event) {
        Entity ent = event.getEntity();
        if(!zombies.isEmpty() && zombies.get(ent.getEntityId()) != null) {
            if(games.getState(zombies.get(ent.getEntityId())) > 1) {
                //REQUIRED STUFF HERE
                String map = zombies.get(ent.getEntityId());
                PayPlayers(ent.getEntityId());
                DamageCause deathreason = ent.getLastDamageCause().getCause();
                if(!drops.isEmpty()) {
                    event.getDrops().clear();
                    for(ItemStack item : randomItem()) {
                        event.getDrops().add(item);
                    }
                }
                if(perk.getPerk(map) == 4) {
                    event.setDroppedExp(25);
                }
                if(fastzombies.containsKey(ent.getEntityId())) {
                    fastzombies.remove(ent.getEntityId());
                }
                if(respawn && deathreason != DamageCause.ENTITY_ATTACK && deathreason != DamageCause.PROJECTILE) {
                    games.setZcount(map, (games.getZcount(map) - 1));
                } else {
                    games.setZslayed(map, games.getZslayed(map) + 1);
                }
                zombies.remove(ent.getEntityId());
                if(games.getZslayed(map) >= (wavemax.get(map))) {
                    getLogger().info(map + " slayed: " + Integer.toString(games.getZslayed(map)) + " wavemax: " + Integer.toString(wavemax.get(map)));
                    NewWave(map);
                }
                //POINT-CASH PAY OUT SYSTEM
                if(ent.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
                    EntityDamageByEntityEvent entitykiller = (EntityDamageByEntityEvent)ent.getLastDamageCause();
                    if(entitykiller.getDamager() instanceof Player || entitykiller.getDamager() instanceof Arrow) {
                        Player player = null;
                        if(entitykiller.getDamager() instanceof Arrow) {
                            Arrow arrow = (Arrow)entitykiller.getDamager();
                            if(arrow.getShooter() instanceof Player) {
                                player = (Player)arrow.getShooter();
                            }
                        } else {
                            player = (Player)entitykiller.getDamager();
                        }
                        try {
                            if(pmethods.playerGame(player).equalsIgnoreCase(map)) {
                                stats.addKills(player.getName(), 1);
                                awards.addKill(player);
                                secondkills.put(map, secondkills.get(map) + 1);
                            }
                        } catch(Exception e) {
                            if(player != null) {
                                player.sendMessage("Naughty boy, back to spawn!");
                                player.teleport(player.getWorld().getSpawnLocation());
                                games.removePlayerMap(player.getName());
                                stats.setPoints(player.getName(), 0);
                                stats.removeSplayerKills(player.getName());
                                dead.remove(player.getName());
                                if(emptyaccount && !points) {
                                    double removem = econ.getBalance(player.getName());
                                    econ.withdrawPlayer(player.getName(), removem);
                                }
                                String name = player.getName();
                                player.setDisplayName(name);
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player killed = event.getEntity();
        if(pmethods.inGame(killed)) {
            String map = pmethods.playerGame(killed);
            getLogger().info(killed.getName() + " has died! Was in game: " + map);
            event.getDrops().clear();
            if(resetpointsdeath) {
                stats.setPoints(killed.getName(), 0);
            }
            games.setPcount(map, (games.getPcount(map) - 1));
            dead.put(killed.getName(), map);
            revive.createRevive(killed);
            stats.addDeaths(killed.getName(), 1);
            if(!points && deathloss > 0) {
                double original = econ.getBalance(killed.getName());
                double withdraw = (original * deathloss);
                econ.withdrawPlayer(killed.getName(), withdraw);
            }
            if(killed.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent killer = (EntityDamageByEntityEvent)killed.getLastDamageCause();
                if(killer.getDamager() instanceof Zombie) {
                    killed.sendMessage(ChatColor.DARK_PURPLE + "You Died Nobly!");
                }
                if(killer.getDamager() instanceof Player) {
                    killed.sendMessage(ChatColor.DARK_PURPLE + "You Died By Treason!");
                }
            } else {
                killed.sendMessage(ChatColor.DARK_PURPLE + "You Died!");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(final PlayerRespawnEvent event) {
        final Player player = event.getPlayer();
        final String name = player.getName();
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                if(pmethods.inGame(player)) {
                    String map = pmethods.playerGame(player);
                    GamesOver(map, false);
                    if(games.getState(map) > 1) {
                        if(dead.containsKey(name)) {
                            if(spawn.spectate.get(map) != null && spectateallow) {
                                player.teleport(spawn.spectate.get(map));
                                player.setAllowFlight(true);
                                player.setFlying(true);
                                spectate.spectators.put(name, map);
                                utilities.hidePlayer(player);
                                player.getInventory().clear();
                                player.getInventory().setArmorContents(null);
                            } else if(deathPoints.get(map) != null && !spectateallow) {
                                player.teleport(deathPoints.get(map));
                            } else {
                                player.teleport(player.getWorld().getSpawnLocation());
                                player.sendMessage("No waiting lobby defined and spectating disabled. Back to spawn!");
                            }
                        }
                    }
                }
            }

        });
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        String[] lines = event.getLines();
        if(lines[0].equalsIgnoreCase("zombie stats") && (Maps.containsKey(lines[1]) || Maps.containsKey(lines[2]))) {
            Player player = event.getPlayer();
            event.setLine(0, "§dzombie stats");
            if(player.hasPermission("zs.edit")) {
                Location sloc = event.getBlock().getLocation();
                Signs.add(sloc);
                saveSigns();
                player.sendMessage(ChatColor.GREEN + "You have created a new ZombieSurvival sign!");
            } else {
                event.setLine(0, "§4MUST BE OP");
            }
        } else if(lines[0].equalsIgnoreCase("zombie stats")) {
            event.setLine(0, "§4BAD SIGN");
        }
        if(lines[0].equalsIgnoreCase("zombie door") && !lines[2].isEmpty() && Maps.containsKey(lines[1])) {
            Player player = event.getPlayer();
            event.setLine(0, "§9zombie door");
            if(player.hasPermission("zs.edit")) {
                player.sendMessage(ChatColor.GREEN + "You have created a new ZombieSurvival sign!");
            } else {
                event.setLine(0, "§4MUST BE OP");
            }
        } else if(lines[0].equalsIgnoreCase("zombie door")) {
            event.setLine(0, "§4BAD SIGN");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        onlinep++;
        Player player = event.getPlayer();
        String name = player.getName();
        if(!pmethods.inGame(player)) {
            if(invsave) {
                try {
                    player.getInventory().setContents(playerdata.get(name).inventory);
                    player.getInventory().setArmorContents(playerdata.get(name).armor);
                } catch(Exception e) {
                }
            }
            stats.setPoints(name, 0);
            player.setHealth(20);
            player.setFoodLevel(20);
            utilities.unhidePlayer(player);
            player.setDisplayName(name);
            if(forcespawn) {
                player.teleport(player.getWorld().getSpawnLocation());
            }
        }
        if(forceclear && !twomintimer.contains(name)) {
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
        }
        if(pmethods.inGame(player)) {
            String map = pmethods.playerGame(player);
            if(games.getState(map) < 2) {
                player.getInventory().clear();
                player.getInventory().setArmorContents(null);
                player.teleport(player.getWorld().getSpawnLocation());
                games.removePlayerMap(name);
                stats.setPoints(name, 0);
                stats.removeSplayerKills(name);
                dead.remove(name);
                if(emptyaccount && !points) {
                    double removem = econ.getBalance(name);
                    econ.withdrawPlayer(name, removem);
                }
                player.setDisplayName(name);
            }
        }
        if(player.isOp() && nvchecker) {
            Version();
            if(outofdate) {
                player.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.RED + "OUT OF DATE! UPDATE AVAILABLE");
            }
        }
        if(antigreif) {
            player.sendMessage(ChatColor.GREEN + "Right click a sign or type /join to join a ZombieSurvival game!");
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        onlinep--;
        final Player player = event.getPlayer();
        final String name = player.getName();
        revive.removeSign(name);
        if(pmethods.inGame(player)) {
            twomintimer.add(name);
            final String map = pmethods.playerGame(player);
            if(pmethods.onlinepcount(map) > 1) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        if(!player.isOnline()) {
                            if(!points && emptyaccount) {
                                double original = econ.getBalance(name);
                                econ.withdrawPlayer(name, original);
                            } else if(!points && deathloss > 0) {
                                double original = econ.getBalance(name);
                                double withdraw = (original * deathloss);
                                econ.withdrawPlayer(name, withdraw);
                            }
                            if(!dead.containsKey(name)) {
                                int tempcount = games.getPcount(map);
                                games.setPcount(map, (tempcount - 1));
                            }
                            games.removePlayerMap(name);
                            stats.removeSplayerPoints(name);
                            twomintimer.remove(name);
                            dead.remove(name);
                            GamesOver(map, false);
                        }
                    }

                }, 1200);
            } else {
                if(!points && emptyaccount) {
                    double original = econ.getBalance(name);
                    econ.withdrawPlayer(name, original);
                } else if(!points && deathloss > 0) {
                    double original = econ.getBalance(name);
                    double withdraw = (original * deathloss);
                    econ.withdrawPlayer(name, withdraw);
                }
                if(!dead.containsKey(name)) {
                    int tempcount = games.getPcount(map);
                    games.setPcount(map, (tempcount - 1));
                }
                games.removePlayerMap(name);
                stats.removeSplayerPoints(name);
                twomintimer.remove(name);
                dead.remove(player.getName());
                GamesOver(map, false);
            }
        }
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        onlinep--;
        final Player player = event.getPlayer();
        final String name = player.getName();
        revive.removeSign(name);
        if(pmethods.inGame(player)) {
            twomintimer.add(name);
            final String map = pmethods.playerGame(player);
            if(pmethods.onlinepcount(map) > 1) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        if(!player.isOnline()) {
                            if(!points && emptyaccount) {
                                double original = econ.getBalance(name);
                                econ.withdrawPlayer(name, original);
                            } else if(!points && deathloss > 0) {
                                double original = econ.getBalance(name);
                                double withdraw = (original * deathloss);
                                econ.withdrawPlayer(name, withdraw);
                            }
                            if(!dead.containsKey(name)) {
                                int tempcount = games.getPcount(map);
                                games.setPcount(map, (tempcount - 1));
                            }
                            games.removePlayerMap(name);
                            stats.removeSplayerPoints(name);
                            twomintimer.remove(name);
                            dead.remove(name);
                            GamesOver(map, false);
                        }
                    }

                }, 200);
            } else {
                if(!points && emptyaccount) {
                    double original = econ.getBalance(name);
                    econ.withdrawPlayer(name, original);
                } else if(!points && deathloss > 0) {
                    double original = econ.getBalance(name);
                    double withdraw = (original * deathloss);
                    econ.withdrawPlayer(name, withdraw);
                }
                if(!dead.containsKey(name)) {
                    int tempcount = games.getPcount(map);
                    games.setPcount(map, (tempcount - 1));
                }
                games.removePlayerMap(name);
                stats.removeSplayerPoints(name);
                twomintimer.remove(name);
                dead.remove(player.getName());
                GamesOver(map, false);
            }
        }
    }

    @EventHandler
    public void onPlayerPickup(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        if(dead.containsKey(player.getName())) {
            event.setCancelled(true);
        }
    }

    public void joinItems(Player player) {
        if(!player.hasPermission("zs.donator") && invsave) {
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
        }
        if(!joinitems.isEmpty() && itemsatjoin && !classes.isClassed(player)) {
            for(ItemStack item : joinarmor) {
                utilities.equipPlayer(player, item);
                player.updateInventory();
            }
            for(ItemStack item : joinitems) {
                utilities.equipPlayer(player, item);
                player.updateInventory();
            }
            player.sendMessage(ChatColor.GREEN + "Check Inventory for Items!");
        }
    }

    public void Version() {
        try {
            // Create a URL for the desired page
            URL url = new URL("https://dl.dropbox.com/u/34805710/zsinfo.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while((str = in.readLine()) != null) {
                if(str.equals(VERSION)) {
                    getLogger().info("Version is current!");
                } else {
                    Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.RED + "OUT OF DATE! UPDATE AVAILABLE");
                    Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.RED + "Newest Version: " + str + " Your Version: " + VERSION);
                    outofdate = true;
                }
            }
        } catch(MalformedURLException e) {
        } catch(IOException e) {
        }
    }

    private boolean setupEconomy() {
        if(getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if(rsp
                == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ
                != null;
    }

    public List<ItemStack> randomItem() {
        List<ItemStack> items = new ArrayList<ItemStack>();
        for(ItemStack i : drops.keySet()) {
            double chance = drops.get(i);
            double chan = random.nextDouble();
            if(chan <= chance) {
                items.add(i);
            }
        }
        return items;
    }

    public void maxfinder(String map) {
        int mz = games.getMaxZombies(map);
        int w = games.getWave(map);
        if(mz < 10) {
            wavemax.put(map, (int)(mz * w * 0.5));
        }
        if(mz >= 10 && mz <= 50) {
            wavemax.put(map, (int)(mz * w * 0.1));
        }
        if(mz >= 51 && mz <= 100) {
            wavemax.put(map, (int)(mz * w * 0.08));
        }
        if(mz >= 101 && mz <= 200) {
            wavemax.put(map, (int)(mz * w * 0.05));
        }
        if(mz >= 201) {
            wavemax.put(map, (int)(mz * w * 0.04));
        }
        if(wavemax.get(map) < 1) {
            wavemax.put(map, 1);
        }
    }

    public void SignUpdater() {
        for(Iterator<Location> it = Signs.iterator(); it.hasNext();) {
            Location sloc = it.next();
            Block block = sloc.getBlock();
            if(block.getState() instanceof Sign) {
                Sign sign = (Sign)block.getState();
                String[] lines = sign.getLines();
                if(lines[1].contains("zombies")) {
                    String map = lines[2];
                    if(Maps.containsKey(map) && games.getState(map) > 1) {
                        sign.setLine(1, "§4" + Integer.toString(games.getZcount(map) - games.getZslayed(map)) + " §0zombies");
                        sign.setLine(3, "Wave: " + Integer.toString(games.getWave(map)));
                        sign.update();
                    } else {
                        sign.setLine(1, "no zombies in");
                        sign.setLine(3, "not started");
                        sign.update();
                    }
                    if(!Maps.containsKey(map)) {
                        block.setTypeId(0);
                        it.remove();
                        saveSigns();
                    }
                } else if(Maps.containsKey(lines[1])) {
                    sign.setLine(2, "Players: " + Integer.toString(pmethods.numberInMap(lines[1])) + "/" + Integer.toString(games.getMaxPlayers(lines[1])));
                    sign.setLine(3, "Wave: " + Integer.toString(games.getWave(lines[1])));
                    sign.update();
                } else {
                    block.setTypeId(0);
                    it.remove();
                    saveSigns();
                }
            }
        }
    }

    public void saveSigns() {
        List<String> temp = new ArrayList<String>();
        for(Location strloc : Signs) {
            temp.add(parseToStr(strloc));
        }
        getCustomConfig().set("signs", temp);
        saveCustomConfig();
    }

    public void reloadPlayers() {
        Player[] list = Bukkit.getOnlinePlayers();
        for(Player p : list) {
            stats.setPoints(p.getName(), 0);
            stats.setKills(p.getName(), 0);
            onlinep++;
        }
    }

    public void openDoors(Location middle, String map) {
        World world = Bukkit.getWorld(Maps.get(map));
        for(int x = middle.getBlockX() - doorfindradius; x <= middle.getBlockX() + doorfindradius; x++) {
            for(int y = middle.getBlockY() - doorfindradius; y <= middle.getBlockY() + doorfindradius; y++) {
                for(int z = middle.getBlockZ() - doorfindradius; z <= middle.getBlockZ() + doorfindradius; z++) {
                    door.openDoor(door.findDoor(x, y, z));
                }
            }
        }
    }

    public void QuickUpdate() {
        task2 = getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                for(String map : Maps.keySet()) {
                    World world = Bukkit.getWorld(Maps.get(map));
                    if(games.getState(map) > 1 && world != null && games.getPcount(map) > 0) {
                        List<Location> plist = new ArrayList<Location>();
                        for(String pl : pmethods.playersInMap(map)) {
                            Player p = Bukkit.getPlayer(pl);
                            if(!dead.containsKey(pl) && p != null && !p.isDead() && p.getWorld().getName().equalsIgnoreCase(Maps.get(map))) {
                                plist.add(p.getLocation());
                            }
                        }
                        List<LivingEntity> templist = getLivingEnts(world);
                        for(LivingEntity went : templist) {
                            if(went.isValid()) {
                                int id = went.getEntityId();
                                if(zombies.containsKey(id) && zombies.get(id).equalsIgnoreCase(map)) {
                                    Location myLocation = went.getLocation();
                                    bar.damageBars(myLocation, map);
                                    Location closest = spawn.lobbies.get(map);
                                    if(plist.size() > 0) {
                                        closest = plist.get(0);
                                        double closestDist = 999999999999999D;
                                        for(Location locs : plist) {
                                            double dist = locs.distanceSquared(myLocation);
                                            if(dist < closestDist) {
                                                closestDist = dist;
                                                closest = locs;
                                            }
                                        }
                                    }
                                    if(went.getType() == EntityType.WOLF) {
                                        Wolf w = (Wolf)went;
                                        updateTarget(map, w);
                                    }
                                    Location TravelTo = utilities.getSegment(went, closest);
                                    if(fastzombies.containsKey(went.getEntityId()) && fastzombies.get(went.getEntityId()).equalsIgnoreCase(map)) {
                                        utilities.livingEntityMoveTo(went, TravelTo.getX(), TravelTo.getY(), TravelTo.getZ(), ((float)fastseekspeed));
                                    } else {
                                        utilities.livingEntityMoveTo(went, TravelTo.getX(), TravelTo.getY(), TravelTo.getZ(), ((float)seekspeed));
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }, 5, 5);
    }

    public void updateTarget(String map, Wolf w) {
        if((targetSelector != null) && (w instanceof CraftWolf)) {
            CraftWolf cw = (CraftWolf)w;
            EntityWolf ew = cw.getHandle();
            PathfinderGoalSelector s = null;
            try {
                s = (PathfinderGoalSelector)targetSelector.get(ew);
            } catch(Exception e) {
            }
            if(s != null) {
                s.a(4, new PathfinderGoalNearestAttackableTarget(ew, EntityHuman.class, 0, true));
            }
        }
        w.setAngry(true);
    }

    public void PayPlayers(int zombid) {
        try {
            for(String player : zombiescore.get(zombid).keySet()) {
                Player p = Bukkit.getPlayer(player);
                if(p != null) {
                    int newscore = 0;
                    int gained = 0;
                    if(points) {
                        gained = zombiescore.get(zombid).get(player);
                        newscore = (gained + (int)stats.getSesPoints(player));
                        stats.addPoints(player, gained);
                    } else if(!points) {
                        gained = zombiescore.get(zombid).get(player);
                        newscore = (gained + (int)econ.getBalance(player));
                        econ.depositPlayer(player, gained);
                    }
                    if(p != null) {
                        if(points) {
                            p.sendMessage(ChatColor.GREEN + "You now have " + ChatColor.DARK_RED + Integer.toString(newscore) + ChatColor.GREEN + " points! GAINED: " + ChatColor.DARK_RED + Integer.toString(gained));
                        } else if(!points) {
                            String score = String.format("%.1f", econ.getBalance(player));
                            p.sendMessage(ChatColor.GREEN + "You now have " + ChatColor.DARK_RED + score + ChatColor.GREEN + " dollars! GAINED: " + ChatColor.DARK_RED + Integer.toString((int)(zombiescore.get(zombid).get(player) / 2)));
                        }
                    }
                }
            }
        } catch(Exception e) {
        }
    }

    public void saveCustomConfig() {
        if(customConfig == null || customConfigFile == null) {
            return;
        }
        try {
            getCustomConfig().save(customConfigFile);
        } catch(IOException ex) {
            this.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    public FileConfiguration getCustomConfig() {
        if(customConfig == null) {
            this.reloadCustomConfig();
        }
        return customConfig;
    }

    public void reloadCustomConfig() {
        if(customConfigFile == null) {
            customConfigFile = new File(getDataFolder(), "games.yml");
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        // Look for defaults in the jar
        InputStream defConfigStream = this.getResource("games.yml");
        if(defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            customConfig.setDefaults(defConfig);
        }
    }

    public boolean mysteryBox(Location middle, Chest chest, Player p) {
        World world = middle.getWorld();
        for(int x = middle.getBlockX() - 2; x <= middle.getBlockX() + 2; x++) {
            for(int y = middle.getBlockY() - 2; y <= middle.getBlockY() + 2; y++) {
                for(int z = middle.getBlockZ() - 2; z <= middle.getBlockZ() + 2; z++) {
                    Location temp = new Location(world, x, y, z);
                    Block sign = temp.getBlock();
                    if(sign.getState() instanceof Sign) {
                        Sign actual = (Sign)sign.getState();
                        String[] lines = actual.getLines();
                        try {
                            int cost = Integer.parseInt(lines[1]);
                            List<HumanEntity> viewers = chest.getBlockInventory().getViewers();
                            if(viewers.size() > 0) {
                                p.sendMessage("Try again later. Only one person at a time!");
                                return false;
                            }
                            if(lines[0].equalsIgnoreCase("§9zombie box")) {
                                if(points && stats.getSesPoints(p.getName()) >= cost) {
                                    stats.addPoints(p.getName(), -cost);
                                    chest.getBlockInventory().clear();
                                    int ritem = random.nextInt(boxitems.size());
                                    ItemStack item = boxitems.get(ritem);
                                    chest.getBlockInventory().setItem(random.nextInt(27), item);
                                    chest.update();
                                    String name = p.getName();
                                    p.setDisplayName("[" + Double.toString(stats.getSesPoints(p.getName())) + "]" + name);
                                    p.sendMessage(ChatColor.GREEN + "You have purchased this " + ChatColor.DARK_RED + "Mysterybox " + ChatColor.GREEN + "for " + Integer.toString(cost) + " points!");
                                    return true;
                                } else if(!points && econ.getBalance(p.getName()) >= cost) {
                                    econ.withdrawPlayer(p.getName(), cost);
                                    chest.getBlockInventory().clear();
                                    int ritem = random.nextInt(boxitems.size() - 1);
                                    ItemStack item = boxitems.get(ritem);
                                    chest.getBlockInventory().setItem(random.nextInt(27), item);
                                    chest.update();
                                    p.sendMessage(ChatColor.GREEN + "You have purchased this " + ChatColor.DARK_RED + "Mysterybox " + ChatColor.GREEN + "for " + Integer.toString(cost) + " dollars!");
                                    return true;
                                } else {
                                    p.sendMessage("Not enough money to purchase!");
                                    return false;
                                }
                            }
                        } catch(Exception e) {
                            return true;
                        }
                    }
                }
            }
        }
        return true;
    }
    //@TODO FIX THIS NIGRIS!!!!!!!!!!!!!!!!!!!!!!

    public void checkMobs() {
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                if(!zombies.isEmpty()) {
                    for(String map : Maps.keySet()) {
                        if(games.getState(map) == 2) {
                            World world = Bukkit.getWorld(Maps.get(map));
                            List<Entity> templis = getLivingEnts(world);
                            List<Integer> tempID = new ArrayList<Integer>();
                            for(Entity id : templis) {
                                int entid = id.getEntityId();
                                tempID.add(entid);
                            }
                            List<Integer> IDs = new ArrayList<Integer>();
                            for(Integer i : zombies.keySet()) {
                                if(zombies.get(i).equalsIgnoreCase(map)) {
                                    IDs.add(i);
                                }
                            }
                            for(Integer i2 : IDs) {
                                if(!tempID.contains(i2)) {
                                    games.setZslayed(map, (games.getZslayed(map) + 1));
                                    zombies.remove(i2);
                                }
                            }
                            if(games.getZslayed(map) >= (wavemax.get(map))) {
                                NewWave(map);
                            }
                        }
                    }
                }
            }

        }, 200, 200);
    }

    public void AsynchTasks() {
        getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
            public void run() {
                for(Iterator<String> it = justleftgame.keySet().iterator(); it.hasNext();) {
                    String string = it.next();
                    int orig = justleftgame.get(string);
                    if(orig < leavetimer) {
                        orig++;
                        justleftgame.put(string, orig);
                    } else {
                        it.remove();
                    }
                }
            }

        }, 20, 20);
    }

    public void synchTasks() {
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                SignUpdater();
            }

        }, 40, 40);
    }

    public void perpNight() {
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                String t = "";
                for(String s : Maps.keySet()) {
                    String b = Maps.get(s);
                    if(!b.equalsIgnoreCase(t)) {
                        t = b;
                        World world = Bukkit.getWorld(b);
                        if(perpnight) {
                            world.setTime(13000);
                        }
                    }
                }
            }

        }, 5, 8000);
    }

    public void dealDamage() {
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                Player[] list = Bukkit.getOnlinePlayers();
                for(Player p : list) {
                    if(p.getLocation().getBlock().getTypeId() == 8 || p.getLocation().getBlock().getTypeId() == 9 || p.getLocation().getBlock().getTypeId() == 30) {
                        if(pmethods.inGame(p) && infectmat) { //readd inGame(p) for public release
                            p.damage(2);
                        }
                    }
                }
            }

        }, 20, 20);
    }

    public void zsDebug(int i, Player p) {
        if(i == 0) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "-----------Diagnostics Started-----------");
            for(String map : Maps.keySet()) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " state: " + Integer.toString(games.getState(map)));
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " onlinepcount: " + Integer.toString(pmethods.onlinepcount(map)));
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " pcount: " + Integer.toString(games.getPcount(map)));
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " numberinmap: " + Integer.toString(pmethods.numberInMap(map)));
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " wave: " + Integer.toString(games.getWave(map)));
                Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " zcount: " + Integer.toString(games.getZcount(map)));
            }
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Signs: " + Integer.toString(Signs.size()));
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Maps: " + Integer.toString(Maps.size()));
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Zombies: " + Integer.toString(zombies.size()));
            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "------------Diagnostics Ended------------");
        }
        if(i == 1 && p != null) {
            p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "-----------Diagnostics Started-----------");
            for(String map : Maps.keySet()) {
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " state: " + Integer.toString(games.getState(map)));
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " onlinepcount: " + Integer.toString(pmethods.onlinepcount(map)));
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " pcount: " + Integer.toString(games.getPcount(map)));
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " numberinmap: " + Integer.toString(pmethods.numberInMap(map)));
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " wave: " + Integer.toString(games.getWave(map)));
                p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Game: " + map + " zcount: " + Integer.toString(games.getZcount(map)));
            }
            p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Signs: " + Integer.toString(Signs.size()));
            p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Maps: " + Integer.toString(Maps.size()));
            p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "Number of Zombies: " + Integer.toString(zombies.size()));
            p.sendMessage(ChatColor.GREEN + "[ZombieSurvival] " + ChatColor.WHITE + "------------Diagnostics Ended------------");
        }
    }

    public static synchronized List getEnts(World world) {
        return world.getEntities();
    }

    public static synchronized List getLivingEnts(World world) {
        return world.getLivingEntities();
    }

    public void resetDoors(String m) {
        try {
            door.resetDoors(m);
        } catch(Exception e) {
            getLogger().info(m + " Did not reset doors correctly!");
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        String p = e.getPlayer().getName();
        String m = e.getMessage();
        Player pp = e.getPlayer();
        if(easycreate.containsKey(p)) {
            int step = easycreate.get(p);
            switch(step) {
                case 0:
                    ecname.put(p, m);
                    pp.sendMessage(ChatColor.GREEN + "Please type the max players for " + ecname.get(p));
                    easycreate.put(p, 1);
                    e.setCancelled(true);
                    break;
                case 1:
                    try {
                        ecpcount.put(p, Integer.parseInt(m));
                        eczcount.put(p, utilities.calcMaxZ(Integer.parseInt(m)));
                        pp.sendMessage(ChatColor.GREEN + "Please type the max waves for " + ecname.get(p));
                        easycreate.put(p, 2);
                    } catch(Exception ec) {
                        pp.sendMessage(ChatColor.RED + "Try again, could not parse number!");
                    }
                    e.setCancelled(true);
                    break;
                case 2:
                    try {
                        ecwcount.put(p, Integer.parseInt(m));
                        easycreate.put(p, 3);
                        pp.sendMessage(ChatColor.GOLD + "Creating game for " + ecname.get(p) + "!" + ChatColor.DARK_RED + " Is this correct??" + ChatColor.GOLD + " players: " + Integer.toString(ecpcount.get(p)) + " waves: " + Integer.toString(ecwcount.get(p)));
                        pp.sendMessage(ChatColor.GREEN + "Please type y or n (yes or no)");
                    } catch(Exception ex) {
                        pp.sendMessage(ChatColor.RED + "Try again, could not parse number!");
                    }
                    e.setCancelled(true);
                    break;
                case 3:
                    if(m.contains("y")) {
                        pp.performCommand("zs-create " + ecname.get(p) + " " + Double.toString(eczcount.get(p)) + " " + Integer.toString(ecpcount.get(p)) + " " + Integer.toString(ecwcount.get(p)));
                        easycreate.put(p, 4);
                    } else {
                        pp.sendMessage(ChatColor.GREEN + "Cancelling!");
                        easycreate.remove(p);
                        ecpcount.remove(p);
                        eczcount.remove(p);
                        ecwcount.remove(p);
                        ecname.remove(p);
                    }
                    e.setCancelled(true);
                    break;
            }
        }
    }

    public void smartDoors(Block b, String m, int w) {
        if(b.getData() >= 0x8) {
            Block a = b.getRelative(BlockFace.DOWN);
            door.addDoor(b, m, w);
        } else {
            Block a = b.getRelative(BlockFace.UP);
            door.addDoor(b, m, w);
        }
    }

}
